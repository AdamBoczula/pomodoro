# Pomodoro Application

You can find this application deployed here: https://boczkodoro.firebaseapp.com/en

## Stack

- Angular 12.x (I am trying to be up do date with the newest version of angular),
- Angular Material,
- NgRx,
- Dexie,
- Jest (and testing ngrx/effects with Marbles),

## Purpose

I develop this application for upskill purposes. Additionally I like to work in Pomodoro approach. That's the reaseon why I am doing that app.

## Ideas to impplement (in the future)

- PWA support,
- Possibility to create an account, login/logout, ...,
- Change time of pomodoro cycle,
- Preview archived tasks, and see statistics about that,
- Change language (EN/PL/DE (I am thinking about that) ),
