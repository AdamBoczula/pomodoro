import { Component, Inject } from '@angular/core';
import { MatDialogRef ,  MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogData } from '../../models/confirmation-dialog-data.interface';

@Component({
  selector: 'pom-confirmation',
  template: `
    <h1 mat-dialog-title>{{ data.header }}</h1>
    <div mat-dialog-content>
      <p>Do you want to archive this task?</p>
    </div>
    <div mat-dialog-actions>
      <button
        mat-raised-button
        color="primary"
        [mat-dialog-close]="false"
        (click)="onNoClick()"
      >
        No Thanks
      </button>
      <button mat-button [mat-dialog-close]="true">Archive</button>
    </div>
  `,
  styleUrls: ['./confirmation.component.scss'],
})
export class ConfirmationComponent {
  constructor(
    public dialogRef: MatDialogRef<ConfirmationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmationDialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
