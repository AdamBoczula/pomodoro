import { MatDialog, MatDialogConfig  } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ComponentType } from '@angular/cdk/portal';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  constructor(private dialog: MatDialog) {}
  public open(
    component: ComponentType<any>,
    config?: MatDialogConfig
  ): Observable<boolean> {
    const dialogRef = this.dialog.open(component, config);
    return dialogRef.afterClosed();
  }
}
