import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';

@NgModule({
  declarations: [ConfirmationComponent],
  imports: [MatDialogModule, CommonModule, MatButtonModule],
})
export class ModalModule {
  constructor() {}
}
