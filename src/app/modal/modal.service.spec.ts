import { MatDialog } from '@angular/material/dialog';
import { TestBed } from '@angular/core/testing';
import { Subject } from 'rxjs';
import { ModalService } from './modal.service';
import { Component } from '@angular/core';

describe('ModalService', () => {
  let service: ModalService;
  let matDialogMock: any;
  let afterCloseSubject: Subject<boolean>;
  beforeEach(() => {
    afterCloseSubject: new Subject<boolean>();
    matDialogMock = {
      open: jest.fn().mockReturnThis(),
      afterClosed: jest.fn().mockReturnValue(afterCloseSubject),
    };
    TestBed.configureTestingModule({
    providers: [{ provide: MatDialog, useValue: matDialogMock }],
    teardown: { destroyAfterEach: false }
});
    service = TestBed.inject(ModalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  test('open should call open on matDialog', () => {
    service.open(TestingDefaultModalMock);
    expect(matDialogMock.open).toHaveBeenCalledTimes(1);
  });
});

@Component({
  selector: 'pom-testing-default-modal',
  template: '',
})
class TestingDefaultModalMock {}
