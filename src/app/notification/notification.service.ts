import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private isAccess: boolean = false;

  public checkNotificationAccess(): void {
    if (!('Notification' in window)) {
      this.isAccess = false;
    } else if (Notification.permission === 'granted'){
      this.isAccess = true;
    } else {
      Notification.requestPermission().then((permission: NotificationPermission) => {
        if (permission === 'granted') {
          this.isAccess = true;
        }
      });
    }
  }

  public notify() {
    if(this.isAccess) {
      new Notification('Work is done!', {
        icon: './assets/pomodoro-notify-icon.png',
        body: 'You can take a rest for a while ;)!',
      });
    }
  }
}
