import { ApplicationHeightCounterService } from 'src/app/services/application-height-counter.service';
import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { debounceTime } from 'rxjs/operators';
import { fromEvent, Subscription } from 'rxjs';
import { LayoutFacadeService } from './facades/layout-facade.service';
import { NotificationService } from './notification/notification.service';
@Component({
  selector: 'pom-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  public title = 'pomodoro-app';
  public isDarkTheme$ = this.layoutFacadeService.isDarkTheme$;
  @ViewChild('pom-navigation') pomNavigation: ElementRef;

  private onResizeSub: Subscription;
  private favicon: HTMLLinkElement;

  constructor(
    private layoutFacadeService: LayoutFacadeService,
    private applicationHeightCounterService: ApplicationHeightCounterService,
    private notificationService: NotificationService
  ) {}

  public ngOnInit(): void {
    this.favicon = document.querySelector('#favicon')!;
    if (window.matchMedia('(prefers-color-scheme: dark)')?.matches) {
      this.favicon.href = 'favicon-dark.ico';
    }
    this.onResizeSub = fromEvent(window, 'resize')
      .pipe(debounceTime(300))
      .subscribe(() => {
        this.applicationHeightCounterService.setGlobalHeightAppVar();
      });

    this.notificationService.checkNotificationAccess();
  }

  public ngOnDestroy(): void {
    this.onResizeSub.unsubscribe();
  }
}
