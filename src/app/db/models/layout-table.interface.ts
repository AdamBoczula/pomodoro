import { TableModelInterface } from './table-model-interface';
export interface LayoutTableModel extends TableModelInterface {
  id?: string;
  isDarkMode: boolean;
}
