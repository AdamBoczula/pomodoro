import { fakeAsync, tick } from '@angular/core/testing';
import { IndexableType } from 'dexie';
import { DexieWrapperService } from './dexie-wrapper.service';
import { TableWrapper } from './table-wrapper';

interface mockDataModel {
  id: number;
}

class testingTableWrapperClass extends TableWrapper<mockDataModel> {
  public constructor(
    dexieWrapperService: DexieWrapperService,
    tableKey: string
  ) {
    super(dexieWrapperService, tableKey);
  }
}
describe('TableWrapper', () => {
  const tableName = 'tableName';
  let service: TableWrapper<mockDataModel>;
  let tableMock: { get: any; put: any };
  let dexieWrapperServiceMock: DexieWrapperService;
  let indexableKey: IndexableType = 0;

  beforeEach(() => {
    tableMock = {
      put: jest.fn().mockReturnValue(Promise.resolve(indexableKey)),
      get: jest.fn(),
    };
    dexieWrapperServiceMock = {
      getTable: jest.fn().mockReturnValue(tableMock),
      transaction: jest.fn().mockImplementation((_, __, fn) => fn()),
    } as unknown as DexieWrapperService;
    service = new testingTableWrapperClass(dexieWrapperServiceMock, tableName);
  });

  test('should be created', () => {
    expect(service).toBeTruthy();
  });

  test("constructor should getTable with 'tableName' tableKey", () => {
    expect(dexieWrapperServiceMock.getTable).toHaveBeenCalledWith(tableName);
    expect(dexieWrapperServiceMock.getTable).toHaveBeenCalledTimes(1);
  });
  test("read should get table with 'tableName'", () => {
    service.read();
    expect(tableMock.get).toHaveBeenCalledWith(tableName);
    expect(tableMock.get).toHaveBeenCalledTimes(1);
  });
  describe('addAndReturnWithId', () => {
    let data: mockDataModel;
    let returnedData: Promise<mockDataModel>;
    beforeEach(() => {
      data = { id: 999 };
      returnedData = service.add(data);
    });
    test("should call transaction with 'rw' mode", () => {
      expect(
        (dexieWrapperServiceMock.transaction as jest.Mock).mock.calls[0][0]
      ).toEqual('rw');
      expect(dexieWrapperServiceMock.transaction).toHaveBeenCalledTimes(1);
    });
    test('should put data to the table', () => {
      expect(tableMock.put).toHaveBeenCalledWith(data);
      expect(tableMock.put).toHaveBeenCalledTimes(1);
    });
    test('should call table get with key', fakeAsync(() => {
      tick();
      expect(tableMock.get).toHaveBeenCalledWith(indexableKey);
      expect(tableMock.get).toHaveBeenCalledTimes(1);
    }));
  });
});
