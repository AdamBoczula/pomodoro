import { Injectable } from '@angular/core';
import Dexie, { Table } from 'dexie';
import { TodoTask } from '../models/todo-task';
import { LayoutTableModel } from './models/layout-table.interface';

export const dbSchema: { [key: string]: string } = {
  toDoTasks: '++id',
  layout: 'id',
  actualTask: 'id',
  doneTasks: 'id',
  archiveTasks: 'id',
};

export type DbTables = keyof typeof dbSchema;

@Injectable({
  providedIn: 'root',
})
export class DexieWrapperService extends Dexie {
  public toDoTasksTable: Dexie.Table<TodoTask, number>;
  public layoutTable: Dexie.Table<LayoutTableModel, number>;
  public actualTaskTable: Dexie.Table<TodoTask, number>;

  constructor() {
    super('PomodoroApp');
    this.version(1).stores(dbSchema);

    this.layoutTable = this.table('layout');
    this.toDoTasksTable = this.table('toDoTasks');
    this.actualTaskTable = this.table('actualTask');
  }

  public getTable<TableModel>(tableName: DbTables): Table {
    return super.table<TableModel>(tableName as string);
  }
}
