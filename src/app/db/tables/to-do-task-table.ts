import { Injectable } from '@angular/core';
import { TodoTask } from '../../models/todo-task';
import { TableWrapper } from '../table-wrapper';
import { DexieWrapperService } from './../dexie-wrapper.service';

@Injectable({ providedIn: 'root' })
export class ToDoTaskTableClass extends TableWrapper<TodoTask> {
  public constructor(dexieWrapperService: DexieWrapperService) {
    super(dexieWrapperService, 'toDoTask');
  }

  public async removeTask(task: TodoTask): Promise<any> {
    return this.table.where('id').equals(task.id!).delete();
  }

  public getCollection(): Promise<TodoTask[]> {
    return this.table.toArray();
  }
}
