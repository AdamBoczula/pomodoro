import { Injectable } from '@angular/core';
import { TodoTask } from 'src/app/models/todo-task';
import { TableWrapper } from '../table-wrapper';
import { DexieWrapperService } from './../dexie-wrapper.service';

@Injectable({
  providedIn: 'root',
})
export class ToDoTaskTableService extends TableWrapper<TodoTask> {
  public constructor(dexieWrapperService: DexieWrapperService) {
    super(dexieWrapperService, 'toDoTasks');
  }
}
