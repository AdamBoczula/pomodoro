import { TestBed } from '@angular/core/testing';
import { LayoutTableModel } from '../models/layout-table.interface';
import { generateDexieWrapperServiceMock } from './../dexie-wrapper-service-mock';
import { DexieWrapperService } from './../dexie-wrapper.service';
import { LayoutTableService } from './layout-table.service';

describe('LayoutTableService', () => {
  let service: LayoutTableService;
  let dexieWrapperServiceMock: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
    providers: [generateDexieWrapperServiceMock()],
    teardown: { destroyAfterEach: false }
});
    service = TestBed.inject(LayoutTableService);
    dexieWrapperServiceMock = TestBed.inject(DexieWrapperService);
  });

  test('should be created', () => {
    expect(service).toBeTruthy();
  });

  test("should call getTable with 'layout' once", () => {
    expect(dexieWrapperServiceMock.getTable).toHaveBeenCalledWith('layout');
    expect(dexieWrapperServiceMock.getTable).toHaveBeenCalledTimes(1);
  });
});
