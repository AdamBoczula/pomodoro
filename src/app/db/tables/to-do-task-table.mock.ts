import { ToDoTaskTableService } from './to-do-task-table.service';

export const generateToDoTaskTableMock = () => ({
  provide: ToDoTaskTableService,
  useValue: {
    add: jest.fn(),
    read: jest.fn(),
  },
});
