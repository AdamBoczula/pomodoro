import { Injectable } from '@angular/core';
import { LayoutTableModel } from '../models/layout-table.interface';
import { TableWrapper } from '../table-wrapper';
import { DexieWrapperService } from './../dexie-wrapper.service';

@Injectable({
  providedIn: 'root',
})
export class LayoutTableService extends TableWrapper<LayoutTableModel> {
  constructor(dexieWrapperService: DexieWrapperService) {
    super(dexieWrapperService, 'layout');
  }
}
