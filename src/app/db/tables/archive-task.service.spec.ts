import { TestBed } from '@angular/core/testing';

import { ArchiveTaskTableService } from './archive-task.service';

describe('ArchiveTaskTableService', () => {
  let service: ArchiveTaskTableService;

  beforeEach(() => {
    TestBed.configureTestingModule({ teardown: { destroyAfterEach: false } });
    service = TestBed.inject(ArchiveTaskTableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
