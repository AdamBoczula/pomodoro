import { Injectable } from '@angular/core';
import { DexieWrapperService } from '../dexie-wrapper.service';
import { TableWrapper } from '../table-wrapper';
import { TodoTask } from './../../models/todo-task';

@Injectable({
  providedIn: 'root',
})
export class ActualTaskTableService extends TableWrapper<TodoTask> {
  constructor(dexieWrapperService: DexieWrapperService) {
    super(dexieWrapperService, 'actualTask');
  }

  public async get(): Promise<TodoTask | null> {
    const actualTaskArr: TodoTask[] = await this.table.toArray();
    return actualTaskArr.length ? actualTaskArr[0] : null;
  }
}
