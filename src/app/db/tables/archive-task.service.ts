import { Injectable } from '@angular/core';
import { TodoTask } from 'src/app/models/todo-task';
import { DexieWrapperService } from '../dexie-wrapper.service';
import { TableWrapper } from '../table-wrapper';

@Injectable({
  providedIn: 'root',
})
export class ArchiveTaskTableService extends TableWrapper<TodoTask> {
  public constructor(dexieWrapperService: DexieWrapperService) {
    super(dexieWrapperService, 'archiveTasks');
  }
}
