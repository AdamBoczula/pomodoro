import { TestBed } from '@angular/core/testing';

import { DoneTaskTableService } from './done-task-table.service';

describe('DoneTaskTableService', () => {
  let service: DoneTaskTableService;

  beforeEach(() => {
    TestBed.configureTestingModule({ teardown: { destroyAfterEach: false } });
    service = TestBed.inject(DoneTaskTableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
