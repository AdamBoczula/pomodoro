import { TestBed } from '@angular/core/testing';
import { generateDexieWrapperServiceMock } from '../dexie-wrapper-service-mock';
import { DexieWrapperService } from '../dexie-wrapper.service';

import { ActualTaskTableService } from './actual-task-table.service';

describe('ActualTaskTableService', () => {
  let service: ActualTaskTableService;
  let dexieWrapperServiceMock: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
    providers: [generateDexieWrapperServiceMock()],
    teardown: { destroyAfterEach: false }
});
    service = TestBed.inject(ActualTaskTableService);
    dexieWrapperServiceMock = TestBed.inject(DexieWrapperService);
  });

  test('should be created', () => {
    expect(service).toBeTruthy();
  });

  test("should call getTable with 'actualTask' once", () => {
    expect(dexieWrapperServiceMock.getTable).toHaveBeenCalledWith('actualTask');
    expect(dexieWrapperServiceMock.getTable).toHaveBeenCalledTimes(1);
  });
});
