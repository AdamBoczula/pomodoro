import { TestBed } from '@angular/core/testing';
import { generateDexieWrapperServiceMock } from './../dexie-wrapper-service-mock';
import { DexieWrapperService } from './../dexie-wrapper.service';
import { ToDoTaskTableService } from './to-do-task-table.service';

describe('ToDoTaskTableService', () => {
  let service: ToDoTaskTableService;
  let dexieWrapperServiceMock: any;
  beforeEach(() => {
    TestBed.configureTestingModule({
    providers: [generateDexieWrapperServiceMock()],
    teardown: { destroyAfterEach: false }
});
    service = TestBed.inject(ToDoTaskTableService);
    dexieWrapperServiceMock = TestBed.inject(DexieWrapperService);
  });

  test('should be created', () => {
    expect(service).toBeTruthy();
  });

  test("should call getTable with 'toDotasks' once", () => {
    expect(dexieWrapperServiceMock.getTable).toHaveBeenCalledWith('toDoTasks');
    expect(dexieWrapperServiceMock.getTable).toHaveBeenCalledTimes(1);
  });
});
