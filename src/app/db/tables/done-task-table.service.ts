import { DexieWrapperService } from './../dexie-wrapper.service';
import { Injectable } from '@angular/core';
import { TodoTask } from 'src/app/models/todo-task';
import { TableWrapper } from '../table-wrapper';

@Injectable({
  providedIn: 'root',
})
export class DoneTaskTableService extends TableWrapper<TodoTask> {
  public constructor(dexieWrapperService: DexieWrapperService) {
    super(dexieWrapperService, 'doneTasks');
  }
}
