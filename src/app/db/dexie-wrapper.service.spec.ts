import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { TodoTask } from '../models/todo-task';

import { DexieWrapperService } from './dexie-wrapper.service';

describe('DexieWrapperService', () => {
  let service: DexieWrapperService;
  let newTask: TodoTask;

  beforeEach(() => {
    TestBed.configureTestingModule({ teardown: { destroyAfterEach: false } });
    service = TestBed.inject(DexieWrapperService);
  });

  beforeEach(() => {
    newTask = { title: 'title1' };
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
