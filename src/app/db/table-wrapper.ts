import { IndexableType, Table } from 'dexie';
import { DexieWrapperService } from './dexie-wrapper.service';
import { TableModelInterface } from './models/table-model-interface';

export abstract class TableWrapper<TableModel> {
  public tableKey: string;
  protected table: Table;

  constructor(
    private dexieWrapperService: DexieWrapperService,
    tableKey: string
  ) {
    this.table = dexieWrapperService.getTable<TableModel>(tableKey);
    this.tableKey = tableKey;
  }

  public write(
    model: TableModelInterface,
    index?: string
  ): Promise<IndexableType> {
    const i = index ?? model.id;
    return this.table.put(model, i);
  }

  public read(): Promise<TableModel> {
    return this.table.get(this.tableKey);
  }

  public delete(id: IndexableType): Promise<void> {
    return this.table.delete(id);
  }

  public getCollection(): Promise<TableModel[]> {
    return this.table.toArray();
  }

  public add(data: TableModel): Promise<TableModel> {
    return this.dexieWrapperService.transaction('rw', this.table, async () => {
      const key: IndexableType = await this.table.put(data);
      return this.table.get(key);
    });
  }
}
