import { DexieWrapperService } from './dexie-wrapper.service';

export const generateDexieWrapperServiceMock = () => ({
  provide: DexieWrapperService,
  useValue: {
    getTable: jest.fn().mockReturnThis(),
    get: jest.fn().mockReturnValue([]),
    put: jest.fn(),
  },
});
