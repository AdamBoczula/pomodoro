import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SECONDS_PER_MINUTE } from 'src/app/pomodoro-home/containers/actual/constants/pomodoro-time-cycle';
import { TaskTimerService } from './../../pomodoro-home/services/task-timer.service';

@Component({
  selector: 'pom-menu-settings-container',
  template: `
    <h2>Settings</h2>
    <form #settingsForm="ngForm">
      <mat-form-field  appearance="fill">
        <mat-label>Pomodoro cycle time</mat-label>
        <input type="number" matInput [(ngModel)]="settingsModel.pomodoroCycle" [max]="240" name="pomodoroCycle">
        <mat-hint  *ngIf="!settingsForm.hasError('max', 'pomodoroCycle')" align="start">Your actual task will be stopped and you can restart it again (with the new time)</mat-hint>
        <mat-error *ngIf="settingsForm.hasError('max', 'pomodoroCycle')">
          Maximum time for pomodoro cycle is 240 minutes.
        </mat-error>
      </mat-form-field>

      <button mat-raised-button color="accent" type="button" (click)="onSave()">Save settings</button>

    </form>
    `,
  styleUrls: ['./menu-settings-container.component.scss'],
})
export class MenuSettingsContainerComponent {
  @Output()
  public onSidebarClose: EventEmitter<() => void> = new EventEmitter();
  @ViewChild('settingsForm')
  public settingsForm: NgForm;
  public settingsModel = {
    pomodoroCycle: 25,
  };

  constructor(private readonly taskTimerService: TaskTimerService) {}

  public onSave(): void {
    if (!this.settingsForm.form.hasError('max', 'pomodoroCycle')) {
      this.onSidebarClose.emit();
      this.taskTimerService.changePomodoroCycleTime(
        this.settingsModel.pomodoroCycle
      );
    }
  }
}
