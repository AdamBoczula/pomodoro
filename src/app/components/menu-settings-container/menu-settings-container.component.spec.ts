import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuSettingsContainerComponent } from './menu-settings-container.component';

describe('MenuSettingsContainerComponent', () => {
  let component: MenuSettingsContainerComponent;
  let fixture: ComponentFixture<MenuSettingsContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuSettingsContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuSettingsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
