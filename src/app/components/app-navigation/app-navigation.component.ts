import {
  Component, ElementRef, ViewChild
} from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
@Component({
  selector: 'pom-app-navigation',
  templateUrl: './app-navigation.component.html',
  styleUrls: ['./app-navigation.component.scss'],
})
export class AppNavigationComponent {
  public isSidebarOpen = false;
  @ViewChild(MatDrawer)
  public sidebar: ElementRef<MatDrawer>;
  public onMenuClick(): void {
    this.isSidebarOpen = !this.isSidebarOpen;
  }
  public onSidebarClose(): void {
    this.isSidebarOpen = false;
  }
}
