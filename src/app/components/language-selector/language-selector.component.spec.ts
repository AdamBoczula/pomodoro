import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatMenuModule as MatMenuModule } from '@angular/material/menu';
import { provideMockStore } from '@ngrx/store/testing';
import * as fromLayout from '../../store/reducers/layout.reducer';
import { LanguageSelectorComponent } from './language-selector.component';

describe('LanguageSelectorComponent', () => {
  let component: LanguageSelectorComponent;
  let fixture: ComponentFixture<LanguageSelectorComponent>;
  const initialState: Partial<fromLayout.State> = {
    isDarkMode: false,
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LanguageSelectorComponent],
      providers: [provideMockStore({ initialState })],
      imports: [MatMenuModule],
      teardown: { destroyAfterEach: false },
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
