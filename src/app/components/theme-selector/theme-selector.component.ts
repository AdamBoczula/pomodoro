import { Component } from '@angular/core';
import { LayoutFacadeService } from './../../facades/layout-facade.service';

@Component({
  selector: 'pom-theme-selector',
  templateUrl: './theme-selector.component.html',
  styleUrls: ['./theme-selector.component.scss'],
})
export class ThemeSelectorComponent {
  public isDarkMode$;
  constructor(private layoutFacadeService: LayoutFacadeService) {
    this.isDarkMode$ = this.layoutFacadeService.isDarkTheme$;
  }
  public toggleTheme(): void {
    this.layoutFacadeService.toggleApplicationMode();
  }
}
