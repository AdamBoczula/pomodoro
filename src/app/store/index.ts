import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import {
 ActionReducer, ActionReducerMap, MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromPomodoroHome from '../pomodoro-home/store';
import * as fromLayout from './reducers/layout.reducer';
import * as fromTask from './reducers/task.reducer';

export interface State {
  router: RouterReducerState<any>;
  pomodoroHome: fromPomodoroHome.State;
  [fromTask.taskFeatureKey]: fromTask.State;
  [fromLayout.layoutFeatureKey]: fromLayout.State;
}

export const reducers: ActionReducerMap<State> = {
  router: routerReducer,
  pomodoroHome: fromPomodoroHome.reducer,
  [fromTask.taskFeatureKey]: fromTask.reducer,
  [fromLayout.layoutFeatureKey]: fromLayout.reducer,
};

export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return (state, action) => {
    const result = reducer(state, action);
    console.groupCollapsed(action.type);
    console.log('prev state', state);
    console.log('action', action);
    console.log('next state', result);
    console.groupEnd();

    return result;
  };
}

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? [logger]
  : [];
