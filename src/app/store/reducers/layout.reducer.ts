import { createReducer, on } from '@ngrx/store';
import { POMODORO_TIME_CYCLE } from 'src/app/pomodoro-home/containers/actual/constants/pomodoro-time-cycle';
import {
  setDarkMode,
  setLayout,
  setLightMode,
  updatePomodoroCycle,
} from './../actions/layout.actions';

export const layoutFeatureKey = 'layout';

export interface State {
  isDarkMode: boolean;
  pomodoroCycle: number;
}

export const initialState: State = {
  isDarkMode: false,
  pomodoroCycle: POMODORO_TIME_CYCLE,
};

export const reducer = createReducer(
  initialState,
  on(setDarkMode, (state: State) => {
    return {
      ...state,
      isDarkMode: true,
    };
  }),
  on(setLightMode, (state: State) => {
    return {
      ...state,
      isDarkMode: false,
    };
  }),
  on(setLayout, (state: State, { isDarkMode }: Pick<State, 'isDarkMode'>) => {
    return {
      ...state,
      isDarkMode,
    };
  }),
  on(
    updatePomodoroCycle,
    (state: State, { pomodoroCycle }: Pick<State, 'pomodoroCycle'>) => {
      return {
        ...state,
        pomodoroCycle,
      };
    }
  )
);
