// @ts-nocheck
import { TaskState, TodoTask } from 'src/app/models/todo-task'
import { moveToDo, moveToDone, setActualTask, updateDonePomodoroCycle } from '../../store/actions/actual-tasks.actions'
import { addToDoTaskSuccess } from '../actions/todo-tasks.actions'
import { setActualTaskSuccess } from './../actions/actual-tasks.actions'
import { archiveTaskSuccess } from './../actions/archive-task.actions'
import { addDoneTask, setDoneTasks } from './../actions/done-tasks.actions'
import { setToDoTasks } from './../actions/todo-tasks.actions'
import { initialState, reducer } from './task.reducer'

describe('Task Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any

      const result = reducer(initialState, action)

      expect(result).toBe(initialState)
    })
  })

  test('handle addToDoTaskSuccess should not add task to the store', () => {
    const toDoTask: TodoTask = {
      title: 'Title',
    } as TodoTask

    const action = new addToDoTaskSuccess({ toDoTask })
    const result = reducer(initialState, action)

    expect(result).toEqual({ ...initialState, toDoTasks: [toDoTask] })
  })

  test('handle setActualTask should add actual task', () => {
    const actualTask: TodoTask = {
      title: 'Title',
      id: '0',
      state: TaskState.TODO,
    }
    const otherTask: TodoTask = {
      title: 'OtherTask Title',
      id: '1',
      state: TaskState.TODO,
    }

    const action = new setActualTask({ actualTask })
    const result = reducer({ ...initialState, actualTask, toDoTasks: [actualTask, otherTask] }, action)

    expect(result).toEqual({
      ...initialState,
      toDoTasks: [otherTask],
      actualTask: actualTask,
    })
  })
  test('handle setActualTask should add actual task', () => {
    const actualTask: TodoTask = {
      title: 'Title',
      id: '0',
      state: TaskState.DONE,
    }
    const otherTask: TodoTask = {
      title: 'OtherTask Title',
      id: '1',
      state: TaskState.TODO,
    }

    const action = new setActualTask({ actualTask })
    const result = reducer(
      {
        ...initialState,
        actualTask,
        toDoTasks: [otherTask],
        doneTasks: [actualTask],
      },
      action
    )

    expect(result).toEqual({
      ...initialState,
      toDoTasks: [otherTask],
      actualTask: actualTask,
    })
  })

  test('handle moveToDo should remove actualTask', () => {
    const actualTask: TodoTask = {
      title: 'Title',
      id: '0',
      state: TaskState.ACTUAL,
    }

    const action = new moveToDo({ task: actualTask })
    const result = reducer({ ...initialState, actualTask }, action)

    expect(result).toEqual({
      toDoTasks: [],
      actualTask: null,
      doneTasks: [],
    })
  })
  test('handle moveToDo should remove doneTasks', () => {
    const doneTask: TodoTask = {
      title: 'Title',
      id: '0',
      state: TaskState.DONE,
    }

    const action = new moveToDo({ task: doneTask })
    const result = reducer({ ...initialState, doneTasks: [doneTask] }, action)

    expect(result).toEqual({
      doneTasks: [],
      actualTask: null,
      toDoTasks: [],
    })
  })

  test('handle moveToDone should remove actualTask', () => {
    const actualTask: TodoTask = {
      title: 'Title',
      id: '0',
      state: TaskState.ACTUAL,
    }

    const action = new moveToDone({ task: actualTask })
    const result = reducer({ ...initialState, actualTask }, action)

    expect(result).toEqual({
      ...initialState,
      actualTask: null,
    })
  })
  test('handle moveToDone should remove actualTask', () => {
    const doneTask: TodoTask = {
      title: 'Title',
      id: '0',
      state: TaskState.DONE,
    }

    const action = new moveToDone({ task: doneTask })
    const result = reducer({ ...initialState, doneTasks: [doneTask] }, action)

    expect(result).toEqual({
      ...initialState,
      doneTasks: [],
    })
  })

  test('handle addDoneTask should add done task to the array', () => {
    const doneTask: TodoTask = {
      title: 'Title',
      id: '0',
    }
    const toDoTask: TodoTask = { title: 'T1', id: '1' }

    const action = new addDoneTask({ doneTask })
    const result = reducer({ doneTasks: [], actualTask: null, toDoTasks: [toDoTask] }, action)

    expect(result).toEqual({
      doneTasks: [doneTask],
      actualTask: null,
      toDoTasks: [toDoTask],
    })
  })

  test('handle setDoneTasks should add done task to the array', () => {
    const doneTask: TodoTask = {
      title: 'Title',
      id: '0',
    }
    const toDoTask: TodoTask = { title: 'T1', id: '1' }

    const action = new setDoneTasks({
      doneTasks: [{ ...doneTask }, { ...doneTask }],
    })
    const result = reducer({ doneTasks: [], actualTask: null, toDoTasks: [toDoTask] }, action)

    expect(result).toEqual({
      doneTasks: [doneTask, doneTask],
      actualTask: null,
      toDoTasks: [toDoTask],
    })
  })

  test('handle setToDoTasks should set array of todo tasks', () => {
    const toDoTasks = [
      { title: 'T1', id: '0' },
      { title: 'T2', id: '1' },
    ]

    const action = new setToDoTasks({ toDoTasks })
    const result = reducer(initialState, action)
    expect(result).toEqual(result)
  })

  test('handle setActualTaskSuccess should set actual task', () => {
    const actualTask = { title: 'T1', id: '0' }

    const action = new setActualTaskSuccess({ actualTask })
    const result = reducer(initialState, action)
    expect(result).toEqual(result)
  })

  test('handle archiveTaskSuccess should remove archivedTask from doneTasks', () => {
    const doneTask1 = {
      title: 't1',
      id: '9',
      state: TaskState.DONE,
    }
    const doneTask2 = {
      title: 't1',
      id: '2',
      state: TaskState.DONE,
    }
    const action = new archiveTaskSuccess({ archiveTask: doneTask2 })
    const result = reducer({ ...initialState, doneTasks: [doneTask1, doneTask2] }, action)
    expect(result).toEqual({ ...initialState, doneTasks: [doneTask1] })
  })
  test('handle archiveTaskSuccess with actualTask should remove archivedTask from actualTask', () => {
    const doneTask = {
      title: 't1',
      id: '9',
      state: TaskState.DONE,
    }
    const actualTask = {
      title: 't1',
      id: '9',
      state: TaskState.ACTUAL,
    }

    const toDoTask = {
      title: 't1',
      id: '2',
      state: TaskState.TODO,
    }
    const action = new archiveTaskSuccess({ archiveTask: actualTask })
    const result = reducer({ actualTask: actualTask, toDoTasks: [toDoTask], doneTasks: [doneTask] }, action)
    expect(result).toEqual({
      actualTask: null,
      toDoTasks: [toDoTask],
      doneTasks: [doneTask],
    })
  })
  test('handle archiveTaskSuccess should remove archivedTask from doneTasks', () => {
    const doneTask1 = {
      title: 't1',
      id: '9',
      state: TaskState.DONE,
    }
    const doneTask2 = {
      title: 't1',
      id: '2',
      state: TaskState.DONE,
    }
    const action = new archiveTaskSuccess({ archiveTask: doneTask2 })
    const result = reducer({ ...initialState, doneTasks: [doneTask1, doneTask2] }, action)
    expect(result).toEqual({ ...initialState, doneTasks: [doneTask1] })
  })
  test('handle updateDonePomodoroCycle', () => {
    const actualTask = {
      title: 't1',
      id: '1',
      state: TaskState.ACTUAL,
      estimate: 3,
      donePomodoroCycles: 1,
    } as TodoTask

    const action = new updateDonePomodoroCycle()
    const result = reducer({ ...initialState, actualTask }, action)
    expect(result).toEqual({
      ...initialState,
      actualTask: {
        ...actualTask,
        donePomodoroCycles: actualTask.donePomodoroCycles + 1,
      },
    })
  })
})
