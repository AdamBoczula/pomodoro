import { addDoneTask, setDoneTasks } from '../actions/done-tasks.actions';
import { addToDoTaskSuccess } from './../actions/todo-tasks.actions';
import { archiveTaskSuccess } from './../actions/archive-task.actions';
import { createReducer, on } from '@ngrx/store';
import { setToDoTasks } from '../actions/todo-tasks.actions';
import { TodoTask } from 'src/app/models/todo-task';
import {
  moveToDo,
  moveToDone,
  setActualTask,
  setActualTaskSuccess,
  updateDonePomodoroCycle,
} from './../actions/actual-tasks.actions';

export const taskFeatureKey = 'task';

export interface State {
  toDoTasks: TodoTask[];
  actualTask: TodoTask | null;
  doneTasks: TodoTask[];
}

export const initialState: State = {
  toDoTasks: [],
  actualTask: null,
  doneTasks: [],
};

export const reducer = createReducer(
  initialState,
  on(addToDoTaskSuccess, (s, { toDoTask }) => {
    return {
      ...s,
      toDoTasks: [...s.toDoTasks, toDoTask],
    };
  }),
  on(addDoneTask, (s, { doneTask }) => {
    return {
      ...s,
      doneTasks: [...s.doneTasks, doneTask],
    };
  }),
  on(setToDoTasks, (s, { toDoTasks }) => ({ ...s, toDoTasks })),
  on(setActualTask, (s, { actualTask }) => {
    return removeTaskFromStore(actualTask, s);
  }),
  on(setDoneTasks, (s, { doneTasks }) => ({ ...s, doneTasks })),
  on(setActualTaskSuccess, (s, { actualTask }) => ({ ...s, actualTask })),
  on(moveToDo, moveToDone, (s, { task }) => {
    return removeTaskFromStore(task, s);
  }),
  on(archiveTaskSuccess, (s, { archiveTask }) => {
    return removeTaskFromStore(archiveTask, s);
  }),
  on(updateDonePomodoroCycle, s => {
    const actualTask = { ...s.actualTask! };
    actualTask.donePomodoroCycles!++;
    return {
      ...s,
      actualTask,
    };
  })
);
function removeTaskFromStore(task: TodoTask, s: State) {
  switch (task.state) {
    case 'Actual':
      return {
        ...s,
        actualTask: null,
      };
    case 'To do':
      return {
        ...s,
        toDoTasks: s.toDoTasks.filter(t => t.id !== task.id),
      };
    default:
      return {
        ...s,
        doneTasks: s.doneTasks.filter(t => t.id !== task.id),
      };
  }
}
