import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const moveTopBottomAnimation = trigger('topBottom', [
  state(
    'top',
    style({
      transform: 'translateY(-250px)',
      opacity: 0,
      zIndex: '-1',
    })
  ),
  state(
    'bottom',
    style({
      transform: 'translateY(0)',
      opacity: 1,
      zIndex: '1',
    })
  ),
  transition('top => bottom', [animate('250ms ease-out')]),
  transition('bottom => top', [animate('250ms ease-out')]),
]);
