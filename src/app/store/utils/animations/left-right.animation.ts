import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const moveRightLeftAnimation = trigger('rightLeft', [
  state(
    'right',
    style({
      transform: 'translateX(-100%)',
      opacity: 0,
      zIndex: '-1',
    })
  ),
  state(
    'left',
    style({
      transform: 'translateX(0)',
      opacity: 1,
      zIndex: '1',
    })
  ),
  transition('right => left', [animate('250ms ease-out')]),
  transition('left => right', [animate('250ms ease-out')]),
]);
