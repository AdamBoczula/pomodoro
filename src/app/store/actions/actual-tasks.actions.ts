import { createAction, props } from '@ngrx/store';
import { TodoTask } from 'src/app/models/todo-task';

export const setActualTask = createAction(
  '[ActualTask] Set Actual Task',
  props<{ actualTask: TodoTask }>()
);

export const setActualTaskSuccess = createAction(
  '[ActualTask] Set Actual Task Success',
  props<{ actualTask: TodoTask }>()
);

export const setActualTaskAsToDo = createAction(
  '[ActualTask] Set Actual Task As ToDo',
  props<{ previousActualTask: TodoTask; actualTask: TodoTask }>()
);

export const removeActualTaskSuccess = createAction(
  '[ActualTask] Remove Actual Task Success'
);

export const updateDonePomodoroCycle = createAction(
  '[ActualTask] Update Done Pomodoro Cycle'
);

export const moveToDo = createAction(
  '[ActualTask] Move Move To Do',
  props<{ task: TodoTask }>()
);
export const moveToDone = createAction(
  '[ActualTask] Move To Done',
  props<{ task: TodoTask }>()
);
