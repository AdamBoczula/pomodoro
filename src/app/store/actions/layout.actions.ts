import { props, createAction } from '@ngrx/store';

export const setLayout = createAction(
  '[Layout] Set Layout',
  props<{ isDarkMode: boolean }>()
);

export const setLayoutSuccess = createAction('[Layout] Set Layout Success');

export const setLightMode = createAction('[Layout] Set Light Mode');

export const setDarkMode = createAction('[Layout] Set Dark Mode');

export const updatePomodoroCycle = createAction(
  '[Layout] Update Pomodoro Cycle',
  props<{ pomodoroCycle: number }>()
);

export const updatePomodoroCycleSuccess = createAction(
  '[Layout] Update Pomodoro Cycle Success'
);
