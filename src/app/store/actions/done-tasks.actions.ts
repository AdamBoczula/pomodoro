import { TodoTask } from './../../models/todo-task';
import { createAction, props } from '@ngrx/store';

export const setDoneTasks = createAction(
  '[DoneTasks] Set Done Tasks',
  props<{ doneTasks: TodoTask[] }>()
);

export const addDoneTask = createAction(
  '[DoneTasks] Add Done Task',
  props<{ doneTask: TodoTask }>()
);

export const addDoneTaskSuccess = createAction(
  '[DoneTasks] Add Done Task Success'
);
