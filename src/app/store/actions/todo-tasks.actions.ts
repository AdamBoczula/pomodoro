import { createAction, props } from '@ngrx/store';
import { TodoTask } from 'src/app/models/todo-task';

export const loadToDoTasks = createAction('[Tasks] Load ToDo Tasks');

export const loadToDoTasksSuccess = createAction(
  '[Tasks] Load ToDo Tasks Success',
  props<{ data: any }>()
);

export const loadToDoTasksFailure = createAction(
  '[Tasks] Load ToDo Tasks Failure',
  props<{ error: any }>()
);

export const addToDoTask = createAction(
  '[Task] Add To Do Task',
  props<{ toDoTask: TodoTask }>()
);

export const addToDoTaskSuccess = createAction(
  '[Task] Add To Do Task Success',
  props<{ toDoTask: TodoTask }>()
);

export const setToDoTasks = createAction(
  '[Task] Set ToDo Tasks',
  props<{ toDoTasks: TodoTask[] }>()
);

export const switchActualWithToDoTask = createAction(
  '[Task] Switch Actual with To Do Task',
  props<{ newActualTask: TodoTask; previousActualTask: TodoTask }>()
);
