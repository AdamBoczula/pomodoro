import { createAction, props } from '@ngrx/store';
import { TodoTask } from 'src/app/models/todo-task';

export const archiveTask = createAction(
  '[ArchiveTask] Archive Task',
  props<{ archiveTask: TodoTask }>()
);

export const archiveTaskSuccess = createAction(
  '[ArchiveTask] Archive Task Success',
  props<{ archiveTask: TodoTask }>()
);
