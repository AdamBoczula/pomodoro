import * as fromLayout from '../reducers/layout.reducer';
import { Action, Store } from '@ngrx/store';
import {
  Actions, createEffect, ofType
} from '@ngrx/effects';
import { DomManipulator } from 'src/app/dom-manipulator/dom-manipulator';
import { Injectable } from '@angular/core';
import { LayoutTableModel } from './../../db/models/layout-table.interface';
import { LayoutTableService } from './../../db/tables/layout-table.service';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { OverlayContainer } from '@angular/cdk/overlay';
import {
  setDarkMode,
  setLayout,
  setLayoutSuccess,
  setLightMode,
  updatePomodoroCycle,
} from './../actions/layout.actions';
@Injectable()
export class LayoutEffects {
  public setLightMode$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(setLightMode),
      map(() => {
        this.layoutTableService.write({
          isDarkMode: false,
          id: 'layout',
        } as LayoutTableModel);
        return setLayout({ isDarkMode: false });
      })
    )
  );

  public setDarkMode$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(setDarkMode),
      map(() => {
        this.layoutTableService.write({
          isDarkMode: true,
          id: 'layout',
        } as LayoutTableModel);
        return setLayout({ isDarkMode: true });
      })
    )
  );

  public setLayout$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(setLayout),
      map(({ isDarkMode }) => {
        isDarkMode
          ? this.overlayContainer
            .getContainerElement()
            .classList.add('dark-theme')
          : this.overlayContainer
            .getContainerElement()
            .classList.remove('dark-theme');

        return setLayoutSuccess();
      })
    )
  );

  public setLayoutSuccess$: Observable<Action> = createEffect(
    () =>
      this.actions$.pipe(
        ofType(setLayoutSuccess),
        tap(() => {
          DomManipulator.showApplication();
        })
      ),
    { dispatch: false }
  );

  // public updatePomodoroCycle: Observable<Action> = createEffect(
  //   () =>
  //     this.actions$.pipe(
  //       ofType(updatePomodoroCycle),

  //     )
  // );

  constructor(
    private actions$: Actions,
    private layoutTableService: LayoutTableService,
    private store: Store<fromLayout.State>,
    private overlayContainer: OverlayContainer
  ) { }
}
