import { TestBed } from '@angular/core/testing';
import { MatDialog  } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import {
  moveToDo,
  moveToDone,
  setActualTask,
  updateDonePomodoroCycle,
} from '../actions/actual-tasks.actions';
import { addToDoTask, addToDoTaskSuccess } from '../actions/todo-tasks.actions';
import * as fromTask from '../reducers/task.reducer';
import { selectActualTask } from '../selectors/task.selectors';
import { AudioService } from './../../audio.service';
import { ActualTaskTableService } from './../../db/tables/actual-task-table.service';
import { ArchiveTaskTableService } from './../../db/tables/archive-task.service';
import { DoneTaskTableService } from './../../db/tables/done-task-table.service';

import { setActualTaskSuccess } from './../actions/actual-tasks.actions';
import {
  archiveTask,
  archiveTaskSuccess,
} from './../actions/archive-task.actions';
import {
  addDoneTask,
  addDoneTaskSuccess,
} from './../actions/done-tasks.actions';
import { switchActualWithToDoTask } from './../actions/todo-tasks.actions';
import { TaskEffects } from './task.effects';
import { TodoTask, TaskState } from '../../models/todo-task';

describe('TaskEffects', () => {
  let initialState;
  let store: MockStore;
  let actions$: Observable<any>;
  let taskEffects: TaskEffects;
  let scheduler: TestScheduler;
  let toDoTaskTableServiceMock: jasmine.SpyObj<any>;
  let archiveTaskTableServiceMock: jasmine.SpyObj<any>;
  let actualTaskTableServiceMock: jasmine.SpyObj<any>;
  let doneTaskTableServiceMock: jasmine.SpyObj<any>;
  let modalServiceMock: jasmine.SpyObj<any>;
  let snackbarMock: jasmine.SpyObj<any>;
  let audioServiceMock = { playMusic: jest.fn() };

  let toDoTask: TodoTask;
  let actualTask: TodoTask;
  let doneTask: TodoTask;

  beforeEach(() => {
    toDoTask = {
      description: 'Description',
      title: 'Title',
      label: 'Label',
      estimate: 1,
      state: TaskState.TODO,
      donePomodoroCycles: 0,
      id: '3',
    };
    actualTask = {
      id: '9',
      description: 'Description',
      title: 'Title',
      label: 'Label',
      donePomodoroCycles: 1,
      estimate: 3,
      state: TaskState.ACTUAL,
    };
    doneTask = {
      id: '5',
      description: 'Description',
      title: 'Title',
      label: 'Label',
      donePomodoroCycles: 0,
      estimate: 3,
      state: TaskState.DONE,
    };
    modalServiceMock = {
      open: jest.fn().mockReturnValue(Promise.resolve(true)),
    };
    snackbarMock = {
      open: jest.fn(),
    };

    initialState = fromTask.initialState;

    TestBed.configureTestingModule({
    providers: [
        TaskEffects,
        provideMockActions(() => actions$),
        ToDoTaskTableService,
        provideMockStore({
            initialState: { ...initialState, toDoTasks: [toDoTask] },
            selectors: [
                {
                    selector: selectActualTask,
                    value: null,
                },
            ],
        }),
        { provide: MatDialog, useValue: {} },
        { provide: ModalService, useValue: modalServiceMock },
        { provide: MatSnackBar, useValue: snackbarMock },
        { provide: AudioService, useValue: audioServiceMock },
    ],
    teardown: { destroyAfterEach: false }
});

    toDoTaskTableServiceMock = TestBed.inject(ToDoTaskTableService);
    actualTaskTableServiceMock = TestBed.inject(ActualTaskTableService);
    doneTaskTableServiceMock = TestBed.inject(DoneTaskTableService);
    archiveTaskTableServiceMock = TestBed.inject(ArchiveTaskTableService);
    modalServiceMock = TestBed.inject(ModalService);
    snackbarMock = TestBed.inject(MatSnackBar);
    store = TestBed.inject(MockStore);
    store.dispatch = jest.fn();

    taskEffects = TestBed.inject(TaskEffects);
  });

  beforeEach(() => {
    scheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
  });

  beforeEach(() => {
    actualTaskTableServiceMock.delete = jest.fn();
    actualTaskTableServiceMock.write = jest.fn();
    toDoTaskTableServiceMock.delete = jest.fn();
    toDoTaskTableServiceMock.write = jest.fn();
    doneTaskTableServiceMock.add = jest.fn();
    doneTaskTableServiceMock.delete = jest.fn();
    archiveTaskTableServiceMock.add = jest.fn();
  });

  test('should be created', () => {
    expect(taskEffects).toBeTruthy();
  });

  test('addToDoTask$ should return createNewTaskSuccess', () => {
    scheduler.run(({ cold, hot, expectObservable }) => {
      const createNewToDoTaskAction = new addToDoTask({ toDoTask });
      const createNewToDoTaskSuccessAction = new addToDoTaskSuccess({
        toDoTask: { ...toDoTask, id: '0' } as TodoTask,
      });
      actions$ = hot('1ms a', { a: createNewToDoTaskAction });

      toDoTaskTableServiceMock.add = jest
        .fn()
        .mockReturnValue(cold('2ms k|', { k: { ...toDoTask, id: '0' } }));

      const expected$ = '1ms 2ms z';
      expectObservable(taskEffects.addToDoTask$).toBe(expected$, {
        z: createNewToDoTaskSuccessAction,
      });
    });
  });

  describe('setActualTask$', () => {
    test('should return setActualTaskSuccess and remove task from todo table when there is no actual task', () => {
      scheduler.run(({ hot, expectObservable, flush }) => {
        const newActualTask = toDoTask;
        const setActualTaskAction = new setActualTask({
          actualTask: newActualTask,
        });
        const setActualTaskSuccessAction = new setActualTaskSuccess({
          actualTask: { ...newActualTask, state: TaskState.ACTUAL },
        });

        actions$ = hot('1ms a', { a: setActualTaskAction });

        const expected$ = '1ms z';

        expectObservable(taskEffects.setActualTask$).toBe(expected$, {
          z: setActualTaskSuccessAction,
        });
        flush();
        expect(actualTaskTableServiceMock.delete).not.toHaveBeenCalled();
        expect(doneTaskTableServiceMock.delete).not.toHaveBeenCalled();
        expect(toDoTaskTableServiceMock.delete).toHaveBeenCalledTimes(1);
        expect(toDoTaskTableServiceMock.delete).toHaveBeenCalledWith(
          newActualTask.id
        );
      });
    });
    test('should remove task from done table when new task is done', () => {
      scheduler.run(({ hot, expectObservable, flush }) => {
        const newActualTask = doneTask;
        const setActualTaskAction = new setActualTask({
          actualTask: newActualTask,
        });
        const setActualTaskSuccessAction = new setActualTaskSuccess({
          actualTask: { ...newActualTask, state: TaskState.ACTUAL },
        });

        actions$ = hot('1ms a', { a: setActualTaskAction });

        const expected$ = '1ms z';

        expectObservable(taskEffects.setActualTask$).toBe(expected$, {
          z: setActualTaskSuccessAction,
        });
        flush();
        expect(toDoTaskTableServiceMock.delete).not.toHaveBeenCalled();
        expect(actualTaskTableServiceMock.delete).not.toHaveBeenCalled();
        expect(doneTaskTableServiceMock.delete).toHaveBeenCalledTimes(1);
        expect(doneTaskTableServiceMock.delete).toHaveBeenCalledWith(
          newActualTask.id
        );
      });
    });
    test('should return setActualTaskSuccess when there is no actual task', () => {
      scheduler.run(({ hot, expectObservable, flush }) => {
        const id: string = '0';
        const newActualTask = { ...toDoTask, id };
        const setActualTaskAction = new setActualTask({
          actualTask: newActualTask,
        });
        const setActualTaskSuccessAction = new setActualTaskSuccess({
          actualTask: { ...newActualTask, state: TaskState.ACTUAL },
        });

        actions$ = hot('1ms a', { a: setActualTaskAction });

        const expected$ = '1ms z';

        expectObservable(taskEffects.setActualTask$).toBe(expected$, {
          z: setActualTaskSuccessAction,
        });
      });
    });

    test('should return setActualTaskSuccess when there is no actual task', () => {
      scheduler.run(({ hot, expectObservable, flush }) => {
        const id: string = '0';
        const newActualTask = { ...toDoTask, id };
        const setActualTaskAction = new setActualTask({
          actualTask: newActualTask,
        });
        const setActualTaskSuccessAction = new setActualTaskSuccess({
          actualTask: { ...newActualTask, state: TaskState.ACTUAL },
        });

        actions$ = hot('1ms a', { a: setActualTaskAction });

        const expected$ = '1ms z';

        expectObservable(taskEffects.setActualTask$).toBe(expected$, {
          z: setActualTaskSuccessAction,
        });
      });
    });

    test('when actual task exists should return switchActualWithToDoTaskAction', () => {
      let movedTask: TodoTask, previousActualTask: TodoTask;

      scheduler.run(({ hot, expectObservable }) => {
        movedTask = { ...toDoTask, id: '1' };
        previousActualTask = { title: 'T1', id: '0' };

        const setActualTaskAction = new setActualTask({
          actualTask: movedTask,
        });

        const setActualTaskSuccessAction = new switchActualWithToDoTask({
          newActualTask: movedTask,
          previousActualTask: previousActualTask,
        });

        store.overrideSelector(selectActualTask, previousActualTask);
        store.refreshState();

        actions$ = hot('- a', { a: setActualTaskAction });

        const expected$ = '- z';
        expectObservable(taskEffects.setActualTask$).toBe(expected$, {
          z: setActualTaskSuccessAction,
        });
      });
    });
  });

  test('switchActualWithToDoTask should delete actualTask from actalTaskTable', () => {
    scheduler.run(({ hot, expectObservable, flush }) => {
      const newActualTask = toDoTask;
      const previousActualTask = {
        title: 'T1',
        id: '0',
        state: TaskState.ACTUAL,
      };

      const switchActualWithToDoTaskAction = new switchActualWithToDoTask({
        newActualTask,
        previousActualTask,
      });

      const setActualTaskSuccessAction = new setActualTaskSuccess({
        actualTask: { ...newActualTask, state: TaskState.ACTUAL },
      });

      const moveBackwardAction = new moveToDo({
        task: { ...previousActualTask, state: TaskState.TODO },
      });

      actions$ = hot('- a', { a: switchActualWithToDoTaskAction });

      const expected$ = '- (yz)'; // () - emission in the same time
      expectObservable(taskEffects.switchActualWithToDoTask$).toBe(expected$, {
        y: moveBackwardAction,
        z: setActualTaskSuccessAction,
      });

      flush();
      expect(actualTaskTableServiceMock.delete).not.toHaveBeenCalled();
    });
  });

  test('setActualTaskSuccess$ should write actualTaskTableService and return setActualTaskSuccess$', () => {
    scheduler.run(({ hot, expectObservable, flush }) => {
      const setActualTaskSuccessAction = new setActualTaskSuccess({
        actualTask,
      });

      actions$ = hot('1ms a', { a: setActualTaskSuccessAction });

      const expected$ = '1ms z';

      expectObservable(taskEffects.setActualTaskSuccess$).toBe(expected$, {
        z: setActualTaskSuccessAction,
      });
      // expect mocks call expectation
      flush();
      expect(actualTaskTableServiceMock.write).toHaveBeenCalledTimes(1);
      expect(actualTaskTableServiceMock.write).toHaveBeenCalledWith(actualTask);
    });
  });

  describe('moveToDo$', () => {
    test('should delete actualTask from table and add todoTaskAction', () => {
      scheduler.run(({ hot, expectObservable, flush }) => {
        const moveToDoAction = new moveToDo({ task: actualTask });
        const addToDoTaskAction = new addToDoTask({
          toDoTask: { ...actualTask, state: TaskState.TODO },
        });

        actions$ = hot('1ms a', { a: moveToDoAction });

        const expected$ = '1ms z';

        expectObservable(taskEffects.moveToDo$).toBe(expected$, {
          z: addToDoTaskAction,
        });
        flush();
        expect(toDoTaskTableServiceMock.delete).not.toHaveBeenCalled();
        expect(doneTaskTableServiceMock.delete).not.toHaveBeenCalled();
        expect(actualTaskTableServiceMock.delete).toHaveBeenCalledTimes(1);
        expect(actualTaskTableServiceMock.delete).toHaveBeenCalledWith(
          actualTask.id
        );
      });
    });

    test('should delete doneTask from table and add todoTaskAction', () => {
      scheduler.run(({ hot, expectObservable, flush }) => {
        const moveToDoAction = new moveToDo({ task: doneTask });
        const addToDoTaskAction = new addToDoTask({
          toDoTask: { ...doneTask, state: TaskState.TODO },
        });

        actions$ = hot('1ms a', { a: moveToDoAction });

        const expected$ = '1ms z';

        expectObservable(taskEffects.moveToDo$).toBe(expected$, {
          z: addToDoTaskAction,
        });
        flush();
        expect(toDoTaskTableServiceMock.delete).not.toHaveBeenCalled();
        expect(actualTaskTableServiceMock.delete).not.toHaveBeenCalled();
        expect(doneTaskTableServiceMock.delete).toHaveBeenCalledTimes(1);
        expect(doneTaskTableServiceMock.delete).toHaveBeenCalledWith(
          doneTask.id
        );
      });
    });

    test('should be triggered by moveToDo action', () => {
      scheduler.run(({ hot, expectObservable }) => {
        const moveToDoAction = new moveToDo({ task: actualTask });
        const addTodoTaskAction = new addToDoTask({
          toDoTask: { ...actualTask, state: TaskState.TODO },
        });

        actions$ = hot('1ms a', { a: moveToDoAction });

        const expected$ = '1ms z';

        expectObservable(taskEffects.moveToDo$).toBe(expected$, {
          z: addTodoTaskAction,
        });
      });
    });
  });

  describe('moveForward$', () => {
    test('should dispatch addDoneTask action and delete actual task from table', () => {
      scheduler.run(({ hot, expectObservable, flush }) => {
        const moveForwardAction = new moveToDone({
          task: actualTask,
        });

        const addDoneTaskAction = new addDoneTask({
          doneTask: { ...actualTask, state: TaskState.DONE },
        });

        actions$ = hot('1ms a', { a: moveForwardAction });
        const expected$ = '1ms z';

        expectObservable(taskEffects.moveToDone$).toBe(expected$, {
          z: addDoneTaskAction,
        });
        flush();
        expect(actualTaskTableServiceMock.delete).toHaveBeenCalledTimes(1);
        expect(actualTaskTableServiceMock.delete).toHaveBeenCalledWith(
          actualTask.id
        );
      });
    });
  });

  test('addDoneTask should add task to the doneTaskTable and return addDoneTaskSuccessAction', () => {
    scheduler.run(({ hot, cold, expectObservable, flush }) => {
      const addDoneTaskAction = new addDoneTask({ doneTask });
      const addDoneTaskSuccessAction = new addDoneTaskSuccess();

      doneTaskTableServiceMock.add = jest
        .fn()
        .mockReturnValue(cold('2ms k', { k: doneTask }));

      actions$ = hot('1ms a', { a: addDoneTaskAction });
      const expected$ = '1ms 2ms z';

      expectObservable(taskEffects.addDoneTask$).toBe(expected$, {
        z: addDoneTaskSuccessAction,
      });
    });
  });

  describe('archiveTask$', () => {
    test('when confirm should return archiveTaskSuccess', () => {
      scheduler.run(({ cold, hot, expectObservable, flush }) => {
        const archiveTaskAction = new archiveTask({ archiveTask: doneTask });
        const archiveTaskSuccessAction = new archiveTaskSuccess({
          archiveTask: doneTask,
        });

        modalServiceMock.open.mockReturnValue(cold('- k|', { k: true }));

        actions$ = hot('- a', { a: archiveTaskAction });
        const expected$ = '--- z';

        expectObservable(taskEffects.archiveTask$).toBe(expected$, {
          z: archiveTaskSuccessAction,
        });
      });
    });
    test('when cancel should not return archiveTaskSuccess', () => {
      scheduler.run(({ cold, hot, expectObservable, flush }) => {
        const archiveTaskAction = new archiveTask({
          archiveTask: doneTask,
        });

        modalServiceMock.open.mockReturnValue(hot('- k|', { k: false }));

        actions$ = hot('- a', { a: archiveTaskAction });
        const expected$ = '---';

        expectObservable(taskEffects.archiveTask$).toBe(expected$);
      });
    });
  });

  test('archiveTaskSuccess$ shuold return archiveTaskSuccess and call snackbar and remove task from done table ', () => {
    scheduler.run(({ cold, hot, expectObservable, flush }) => {
      const archiveTaskSuccessAction = new archiveTaskSuccess({
        archiveTask: doneTask,
      });

      archiveTaskTableServiceMock.add.mockReturnValue(
        cold('- k|', { k: doneTask })
      );
      modalServiceMock.open.mockReturnValue(cold('-l|', { l: true }));
      doneTaskTableServiceMock.delete = jest
        .fn()
        .mockReturnValue(cold('-m|', { m: null }));

      actions$ = hot('- a', { a: archiveTaskSuccessAction });
      const expected$ = '--- z';

      expectObservable(taskEffects.archiveTaskSuccess$).toBe(expected$, {
        z: null,
      });
      flush();
      expect(archiveTaskTableServiceMock.add).toHaveBeenCalledTimes(1);
      expect(archiveTaskTableServiceMock.add).toHaveBeenCalledWith(doneTask);
      expect(doneTaskTableServiceMock.delete).toHaveBeenCalledTimes(1);
      expect(doneTaskTableServiceMock.delete).toHaveBeenCalledWith(
        doneTask.id!
      );
      expect(snackbarMock.open).toHaveBeenCalledTimes(1);
      expect(snackbarMock.open).toHaveBeenCalledWith(
        'Task is successfully archived',
        'OK',
        {
          panelClass: ['snackbar'],
          duration: 5000,
          verticalPosition: 'top',
        }
      );
    });
  });

  test('archiveTaskSuccess$ when actual task should remove task from actual task table ', () => {
    scheduler.run(({ cold, hot, expectObservable, flush }) => {
      const archiveTaskSuccessAction = new archiveTaskSuccess({
        archiveTask: actualTask,
      });

      archiveTaskTableServiceMock.add.mockReturnValue(
        cold('- k|', { k: actualTask })
      );
      modalServiceMock.open.mockReturnValue(cold('-l|', { l: true }));
      actualTaskTableServiceMock.delete = jest
        .fn()
        .mockReturnValue(cold('-m|', { m: null }));

      actions$ = hot('- a', { a: archiveTaskSuccessAction });
      const expected$ = '--- z';

      expectObservable(taskEffects.archiveTaskSuccess$).toBe(expected$, {
        z: null,
      });
      flush();
      expect(archiveTaskTableServiceMock.add).toHaveBeenCalledTimes(1);
      expect(archiveTaskTableServiceMock.add).toHaveBeenCalledWith(actualTask);
      expect(actualTaskTableServiceMock.delete).toHaveBeenCalledTimes(1);
      expect(actualTaskTableServiceMock.delete).toHaveBeenCalledWith(
        actualTask.id!
      );
      expect(snackbarMock.open).toHaveBeenCalledTimes(1);
      expect(snackbarMock.open).toHaveBeenCalledWith(
        'Task is successfully archived',
        'OK',
        {
          panelClass: ['snackbar'],
          duration: 5000,
          verticalPosition: 'top',
        }
      );
    });
  });

  test('archiveTaskSuccess$ when to do task should remove task from to do task table ', () => {
    scheduler.run(({ cold, hot, expectObservable, flush }) => {
      const archiveTaskSuccessAction = new archiveTaskSuccess({
        archiveTask: toDoTask,
      });

      archiveTaskTableServiceMock.add.mockReturnValue(
        cold('- k|', { k: toDoTask })
      );
      modalServiceMock.open.mockReturnValue(cold('-l|', { l: true }));
      toDoTaskTableServiceMock.delete = jest
        .fn()
        .mockReturnValue(cold('-m|', { m: null }));

      actions$ = hot('- a', { a: archiveTaskSuccessAction });
      const expected$ = '--- z';

      expectObservable(taskEffects.archiveTaskSuccess$).toBe(expected$, {
        z: null,
      });
      flush();
      expect(archiveTaskTableServiceMock.add).toHaveBeenCalledTimes(1);
      expect(archiveTaskTableServiceMock.add).toHaveBeenCalledWith(toDoTask);
      expect(toDoTaskTableServiceMock.delete).toHaveBeenCalledTimes(1);
      expect(toDoTaskTableServiceMock.delete).toHaveBeenCalledWith(
        toDoTask.id!
      );
      expect(snackbarMock.open).toHaveBeenCalledTimes(1);
      expect(snackbarMock.open).toHaveBeenCalledWith(
        'Task is successfully archived',
        'OK',
        {
          panelClass: ['snackbar'],
          duration: 5000,
          verticalPosition: 'top',
        }
      );
    });
  });

  describe('updateDonePomodoroCycle$', () => {
    test('should play beep audio and update actualTask on actualTaskTable', () => {
      store.overrideSelector(selectActualTask, actualTask);
      scheduler.run(({ hot, flush, expectObservable }) => {
        const triggerAction = new updateDonePomodoroCycle();
        actions$ = hot('- a', { a: triggerAction });
        const expected$ = '-';
        expectObservable(taskEffects.updateDonePomodoroCycle$).toBe(expected$);
        flush();
        expect(actualTaskTableServiceMock.write).toHaveBeenCalledTimes(1);
        expect(actualTaskTableServiceMock.write).toHaveBeenCalledWith(
          actualTask
        );
        expect(audioServiceMock.playMusic).toHaveBeenCalledTimes(1);
      });
    });

    test('when donePomodoroCycles is less than estimate should add 1 to donePomodoroCycles', () => {
      store.overrideSelector(selectActualTask, actualTask);
      scheduler.run(({ hot, cold, expectObservable }) => {
        const triggerAction = new updateDonePomodoroCycle();
        actions$ = hot('- a', { a: triggerAction });
        const expected$ = '-';
        expectObservable(taskEffects.updateDonePomodoroCycle$).toBe(expected$);
      });
    });

    test('when donePomodoroCycles is equal to estimate should dispatch moveToDone action', () => {
      actualTask.donePomodoroCycles = actualTask.estimate!;
      store.overrideSelector(selectActualTask, actualTask);
      scheduler.run(({ hot, cold, expectObservable }) => {
        const triggerAction = new updateDonePomodoroCycle();
        const expectedAction = new moveToDone({ task: actualTask });
        actions$ = hot('- a', { a: triggerAction });
        const expected$ = '- z';
        expectObservable(taskEffects.updateDonePomodoroCycle$).toBe(expected$, {
          z: expectedAction,
        });
      });
    });
  });
});
