import { OverlayContainer } from '@angular/cdk/overlay';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import * as fromLayout from '../reducers/layout.reducer';
import { selectIsDarkTheme } from '../selectors/layout.selectors';
import { LayoutTableService } from './../../db/tables/layout-table.service';
import {
  setDarkMode,
  setLayout,
  setLayoutSuccess,
  setLightMode,
} from './../actions/layout.actions';
import { LayoutEffects } from './layout.effects';

describe('LayoutEffects', () => {
  let initialState: Partial<fromLayout.State> = { isDarkMode: false };
  let actions$: Observable<any>;
  let layoutEffects: LayoutEffects;
  let layoutTableServiceMock: LayoutTableService;
  let overlayContainerMock: OverlayContainer;
  let classListMock: DOMTokenList;
  let scheduler: TestScheduler;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        LayoutEffects,
        provideMockStore({
          initialState,
          selectors: [
            {
              selector: selectIsDarkTheme,
              value: initialState.isDarkMode,
            },
          ],
        }),
        provideMockActions(() => actions$),
      ],
      teardown: { destroyAfterEach: false },
    });

    layoutEffects = TestBed.inject(LayoutEffects);
    layoutTableServiceMock = TestBed.inject(LayoutTableService);
    overlayContainerMock = TestBed.inject(OverlayContainer);
  });

  beforeEach(() => {
    scheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
  });

  beforeEach(() => {
    layoutTableServiceMock.write = jest.fn();
    classListMock = {
      add: jest.fn(),
      remove: jest.fn(),
    } as unknown as DOMTokenList;

    overlayContainerMock.getContainerElement = jest
      .fn()
      .mockReturnValue({ classList: classListMock });
  });

  test('should be created', () => {
    expect(layoutEffects).toBeTruthy();
  });

  // test('setLightMode$ should return setLayout', () => {
  //   scheduler.run(({ hot, expectObservable, flush }) => {
  //     const setLightModeAction = new setLightMode();
  //     const createNewTaskSuccessAction = new setLayout({
  //       isDarkMode: false,
  //     });
  //     actions$ = hot('1ms a', { a: setLightModeAction });

  //     const expected$ = '1ms z';
  //     expectObservable(layoutEffects.setLightMode$).toBe(expected$, {
  //       z: createNewTaskSuccessAction,
  //     });
  //     flush();
  //     expect(layoutTableServiceMock.write).toHaveBeenCalledTimes(1);
  //     expect(layoutTableServiceMock.write).toHaveBeenCalledWith({
  //       isDarkMode: false,
  //       id: 'layout',
  //     });
  //   });
  // });

  // test('setDarkMode$ should return setLayout', () => {
  //   scheduler.run(({ hot, expectObservable, flush }) => {
  //     const setDarkModeAction = new setDarkMode();
  //     const createNewTaskSuccessAction = new setLayout({
  //       isDarkMode: true,
  //     });
  //     actions$ = hot('1ms a', { a: setDarkModeAction });

  //     const expected$ = '1ms z';
  //     expectObservable(layoutEffects.setDarkMode$).toBe(expected$, {
  //       z: createNewTaskSuccessAction,
  //     });
  //     flush();
  //     expect(layoutTableServiceMock.write).toHaveBeenCalledTimes(1);
  //     expect(layoutTableServiceMock.write).toHaveBeenCalledWith({
  //       isDarkMode: true,
  //       id: 'layout',
  //     });
  //   });
  // });

  test('setLayout$ should return setLayoutSuccess', () => {
    scheduler.run(({ hot, expectObservable, flush }) => {
      const setLayoutAction = new setLayout({ isDarkMode: true });
      const setLayoutSuccessAction = new setLayoutSuccess();
      actions$ = hot('1ms a', { a: setLayoutAction });

      const expected$ = '1ms z';
      expectObservable(layoutEffects.setLayout$).toBe(expected$, {
        z: setLayoutSuccessAction,
      });
      flush();
      expect(overlayContainerMock.getContainerElement).toHaveBeenCalledTimes(1);
      expect(classListMock.remove).toHaveBeenCalledTimes(0);

      expect(classListMock.add).toHaveBeenCalledTimes(1);
      expect(classListMock.add).toHaveBeenCalledWith('dark-theme');
    });
  });

  test('setLayout$ should return setLayoutSuccess', () => {
    scheduler.run(({ hot, expectObservable, flush }) => {
      const setLayoutAction = new setLayout({ isDarkMode: false });
      const setLayoutSuccessAction = new setLayoutSuccess();
      actions$ = hot('1ms a', { a: setLayoutAction });

      const expected$ = '1ms z';
      expectObservable(layoutEffects.setLayout$).toBe(expected$, {
        z: setLayoutSuccessAction,
      });
      flush();
      expect(overlayContainerMock.getContainerElement).toHaveBeenCalledTimes(1);
      expect(classListMock.add).toHaveBeenCalledTimes(0);
      expect(classListMock.remove).toHaveBeenCalledTimes(1);
      expect(classListMock.remove).toHaveBeenCalledWith('dark-theme');
    });
  });
});
