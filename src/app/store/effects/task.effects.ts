import * as fromTask from '../reducers/task.reducer';
import { Action, Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { concatLatestFrom } from '@ngrx/operators';
import { ActualTaskTableService } from './../../db/tables/actual-task-table.service';
import { addDoneTask, addDoneTaskSuccess } from '../actions/done-tasks.actions';
import { archiveTask } from 'src/app/store/actions/archive-task.actions';
import { archiveTaskSuccess } from './../actions/archive-task.actions';
import { ArchiveTaskTableService } from './../../db/tables/archive-task.service';
import { AudioService } from './../../audio.service';
import { ConfirmationComponent } from './../../modal/components/confirmation/confirmation.component';
import { DoneTaskTableService } from './../../db/tables/done-task-table.service';
import { forkJoin, iif, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ModalService } from './../../modal/modal.service';
import { NotificationService } from 'src/app/notification/notification.service';
import { selectActualTask } from '../selectors/task.selectors';
import { TodoTask } from 'src/app/models/todo-task';
import { ToDoTaskTableService } from './../../db/tables/to-do-task-table.service';
import {
  exhaustMap,
  filter,
  map,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  moveToDo,
  moveToDone,
  setActualTask,
  setActualTaskSuccess,
  updateDonePomodoroCycle,
} from '../actions/actual-tasks.actions';
import {
  addToDoTask,
  addToDoTaskSuccess,
  switchActualWithToDoTask,
} from '../actions/todo-tasks.actions';

@Injectable()
export class TaskEffects {
  public addToDoTask$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(addToDoTask),
      exhaustMap(({ toDoTask }) => {
        return this.toDoTaskTableService.add(toDoTask);
      }),
      map((toDoTask) => {
        return addToDoTaskSuccess({ toDoTask });
      })
    )
  );

  public addDoneTask$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(addDoneTask),
      exhaustMap(({ doneTask }) => {
        return this.doneTaskTableService.add(doneTask);
      }),
      map(() => {
        return addDoneTaskSuccess();
      })
    )
  );

  public setActualTask$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(setActualTask),
      tap(({ actualTask }) => {
        this.removeTaskFromTable(actualTask);
      }),
      withLatestFrom(this.store.select(selectActualTask)),
      exhaustMap(([{ actualTask }, previousActualTask]) =>
        iif(
          () => !!previousActualTask,
          of(
            switchActualWithToDoTask({
              newActualTask: actualTask,
              previousActualTask: previousActualTask!,
            })
          ),
          of(
            setActualTaskSuccess({
              actualTask: { ...actualTask, state: 'Actual' },
            })
          )
        )
      )
    )
  );

  public switchActualWithToDoTask$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(switchActualWithToDoTask),
      switchMap(({ newActualTask, previousActualTask }) => {
        return [
          moveToDo({
            task: { ...previousActualTask, state: 'To do' },
          }),
          setActualTaskSuccess({
            actualTask: { ...newActualTask, state: 'Actual' },
          }),
        ];
      })
    )
  );

  public setActualTaskSuccess$: Observable<Action> = createEffect(
    () =>
      this.actions$.pipe(
        ofType(setActualTaskSuccess),
        tap(({ actualTask }) => {
          this.actualTaskTableService.write(actualTask);
        })
      ),
    { dispatch: false }
  );

  public moveToDo$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(moveToDo),
      map(({ task }) => {
        this.removeTaskFromTable(task);
        return addToDoTask({
          toDoTask: { ...task, state: 'To do' },
        });
      })
    )
  );

  public moveToDone$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(moveToDone),
      map(({ task }) => {
        this.removeTaskFromTable(task);
        return addDoneTask({
          doneTask: { ...task, state: 'Done' },
        });
      })
    )
  );

  public updateDonePomodoroCycle$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(updateDonePomodoroCycle),
      concatLatestFrom(() => this.store.select(selectActualTask)),
      tap(([_, actualTask]) => {
        this.audioService.playMusic();
        this.actualTaskTableService.write(actualTask!);
        this.notificationService.notify();
      }),
      filter(([_, actualTask]) => {
        return actualTask?.donePomodoroCycles! >= actualTask?.estimate!;
      }),
      map(([_, actualTask]) => {
        this.notificationService.notify();
        return moveToDone({ task: actualTask! });
      })
    )
  );

  public archiveTask$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(archiveTask),
      exhaustMap(({ archiveTask }) => {
        return forkJoin({
          open: this.modalService.open(ConfirmationComponent, {
            data: {
              header: 'Archive Task',
            },
          }),
          archiveTask: of(archiveTask),
        });
      }),
      filter(({ open }) => {
        return !!open;
      }),
      map(({ archiveTask }) => {
        return archiveTaskSuccess({ archiveTask });
      })
    )
  );

  public archiveTaskSuccess$: Observable<void> = createEffect(
    () =>
      this.actions$.pipe(
        ofType(archiveTaskSuccess),
        exhaustMap(({ archiveTask }) => {
          return this.archiveTaskTableService.add(archiveTask);
        }),
        exhaustMap((archiveTask) => {
          return this.removeTaskFromTable(archiveTask);
        }),
        tap(() => {
          this.snackBar.open('Task is successfully archived', 'OK', {
            duration: 5000,
            verticalPosition: 'top',
            panelClass: ['snackbar'],
          });
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private toDoTaskTableService: ToDoTaskTableService,
    private actualTaskTableService: ActualTaskTableService,
    private doneTaskTableService: DoneTaskTableService,
    private archiveTaskTableService: ArchiveTaskTableService,
    private modalService: ModalService,
    private snackBar: MatSnackBar,
    private audioService: AudioService,
    private store: Store<fromTask.State>,
    private notificationService: NotificationService
  ) {}

  private removeTaskFromTable(archiveTask: TodoTask) {
    switch (archiveTask.state) {
      case 'Actual':
        return this.actualTaskTableService.delete(archiveTask.id!);
      case 'To do':
        return this.toDoTaskTableService.delete(archiveTask.id!);
      default:
        return this.doneTaskTableService.delete(archiveTask.id!);
    }
  }
}
