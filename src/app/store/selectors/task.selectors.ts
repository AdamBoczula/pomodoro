import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromTasks from '../reducers/task.reducer';

export const getTasksState = createFeatureSelector<fromTasks.State>(
  fromTasks.taskFeatureKey
);

export const selectToDoTasks = createSelector(
  getTasksState,
  state => state.toDoTasks
);

export const selectActualTask = createSelector(
  getTasksState,
  state => state.actualTask
);

export const selectDoneTasks = createSelector(
  getTasksState,
  state => state.doneTasks
);
