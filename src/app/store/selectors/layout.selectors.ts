import * as fromLayout from '../reducers/layout.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const getLayoutState = createFeatureSelector<fromLayout.State>(
  fromLayout.layoutFeatureKey
);

export const selectIsDarkTheme = createSelector(
  getLayoutState,
  (state) => state.isDarkMode
);

export const selectPomodoroCycle = createSelector(
  getLayoutState,
  (state) => state.pomodoroCycle
);
