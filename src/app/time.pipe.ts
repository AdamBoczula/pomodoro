import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'time',
})
export class TimePipe implements PipeTransform {
  transform(value: number): string {
    const date = new Date(value);
    const seconds = ('0' + date.getSeconds()).slice(-2);
    const minutes = ('0' + date.getMinutes()).slice(-2);

    return `${minutes}:${seconds}`;
  }
}
