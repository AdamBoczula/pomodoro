import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AudioService {
  constructor() {}

  public playMusic(): void {
    const audio = new Audio('./assets/mp3/beep.wav');
    const numberOfRepeat = 3;
    let counter: number = 0;
    audio.volume = 0.2;
    audio.play();
    audio.onended = () => {
      counter++;
      if (counter < numberOfRepeat) {
        audio.play();
      }
    };
  }
}
