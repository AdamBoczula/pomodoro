import { NgModule } from '@angular/core';
import { Routes, provideRouter } from '@angular/router';

const appRoutes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./pomodoro-home/pomodoro-home.module').then(
        (m) => m.PomodoroHomeModule
      ),
  },
  {
    path: '**',
    redirectTo: '/home',
  },
];

@NgModule({
  providers: [provideRouter(appRoutes)],
})
export class AppRoutingModule {}
