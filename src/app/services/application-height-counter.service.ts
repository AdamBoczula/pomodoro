import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ApplicationHeightCounterService {
  private vh: number;
  private windowHeight: number;
  private navHeight: number;
  private statusBarHeight: number;
  private todoSectionHeader: number;
  private bottomPadding: number;
  private tasksStatusesHeight: number;

  constructor() {}

  public setGlobalHeightAppVar(): void {
    this.setVH();
    this.bottomPadding = 5 * this.vh;

    this.calculateNavHeight();
    this.calculateStatusBarHeight();
    this.calculateTodoSectionHeader();
    this.setTasksStatusesHeight();
    this.setTodoTasksListHeight();
    this.setDoneTasksListHeight();
  }

  private setTodoTasksListHeight() {
    const todoTaskListheight =
      this.tasksStatusesHeight - this.todoSectionHeader - this.bottomPadding;
    document.documentElement.style.setProperty(
      '--todo-tasks-list-height',
      `${todoTaskListheight}px`
    );
  }

  private setDoneTasksListHeight() {
    const todoTaskListheight = this.tasksStatusesHeight - this.bottomPadding;
    document.documentElement.style.setProperty(
      '--done-tasks-list-height',
      `${todoTaskListheight}px`
    );
  }

  private setTasksStatusesHeight() {
    this.tasksStatusesHeight =
      this.windowHeight - (this.statusBarHeight + this.navHeight);

    document.documentElement.style.setProperty(
      '--tasks-statuses-height',
      `${this.tasksStatusesHeight}px`
    );
  }

  private calculateTodoSectionHeader() {
    this.todoSectionHeader = (
      document.querySelector('.todo-section-header') as HTMLElement
    ).offsetHeight;
  }

  private calculateStatusBarHeight() {
    this.statusBarHeight = (
      document.querySelector('pom-status-bar')! as HTMLElement
    ).offsetHeight;
  }

  private calculateNavHeight() {
    this.navHeight = (
      document.querySelector('mat-toolbar')! as HTMLElement
    ).offsetHeight;
  }

  private setVH(): void {
    this.windowHeight = document.documentElement.clientHeight;
    this.vh = this.windowHeight / 100;
    document.documentElement.style.setProperty('--vh', `${this.vh}px`);
  }
}
