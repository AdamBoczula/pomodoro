import { TestBed } from '@angular/core/testing';

import { ApplicationHeightCounterService } from './application-height-counter.service';

describe('ApplicationHeightCounterService', () => {
  let service: ApplicationHeightCounterService;

  beforeEach(() => {
    TestBed.configureTestingModule({ teardown: { destroyAfterEach: false } });
    service = TestBed.inject(ApplicationHeightCounterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
