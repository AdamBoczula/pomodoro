import { DomManipulator } from './dom-manipulator';
describe('dom-manipulator', () => {
  test('showApplication should remove not-dispplay class from #pomodoro-app', () => {
    const pomodoroAppElementMock = { style: { opacity: '0' } };
    document.getElementById = jest.fn().mockReturnValue(pomodoroAppElementMock);

    DomManipulator.showApplication();

    expect(pomodoroAppElementMock.style.opacity).toEqual('1');
  });
});
