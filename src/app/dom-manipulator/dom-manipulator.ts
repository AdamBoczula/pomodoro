export class DomManipulator {
  static showApplication(): void {
    const pomodoroApp: HTMLElement = document.getElementById('pomodoro-app')!;
    if (pomodoroApp) {
      pomodoroApp.style.opacity = '1';
    }
  }
}
