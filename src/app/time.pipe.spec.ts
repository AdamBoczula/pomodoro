import { MILISECONDS_PER_SECOND } from './pomodoro-home/containers/actual/constants/pomodoro-time-cycle';
import { TimePipe } from './time.pipe';

describe('TimePipe', () => {
  let pipe: TimePipe;

  beforeEach(() => {
    pipe = new TimePipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should change number of miliseconds to time - 00:30', () => {
    const timeInMs = 30 * MILISECONDS_PER_SECOND;
    const displayed: string = pipe.transform(timeInMs);
    expect(`00:30`).toEqual(displayed);
  });

  it('should change number of miliseconds to time - 01:30', () => {
    const timeInMs = 90 * MILISECONDS_PER_SECOND;
    const displayed: string = pipe.transform(timeInMs);
    expect(`01:30`).toEqual(displayed);
  });

  it('should change number of miliseconds to time - 00:00', () => {
    const timeInMs = 0;
    const displayed: string = pipe.transform(timeInMs);
    expect(`00:00`).toEqual(displayed);
  });

  it('should change number of miliseconds to time - 15:00', () => {
    const timeInMs = 900 * MILISECONDS_PER_SECOND;
    const displayed: string = pipe.transform(timeInMs);
    expect(`15:00`).toEqual(displayed);
  });
});
