import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { AppComponent } from './app.component';
import * as fromRoot from './store/reducers/layout.reducer';

describe('AppComponent', () => {
  const initialState: fromRoot.State = {
    isDarkMode: false,
    pomodoroCycle: 0,
  };
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;
  window.matchMedia = jest.fn().mockReturnValue({ matches: true });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent, AppNavigationMockComponent],
      providers: [provideMockStore({ initialState })],
      imports: [RouterTestingModule],
      teardown: { destroyAfterEach: false },
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('when dark theme favicon should be dark theme icon', () => {
    expect(fixture.nativeElement.querySelector('#favicon').href).toContain(
      'favicon-dark.ico'
    );
  });

  it('when light theme favicon should be default icon', () => {
    fixture = TestBed.createComponent(AppComponent);
    window.matchMedia = jest.fn().mockReturnValueOnce({ matches: false });

    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('#favicon').href).toContain(
      'favicon.ico'
    );
  });
});

@Component({
  selector: 'pom-app-navigation',
  template: '<a href="favicon.ico" id="favicon">',
})
class AppNavigationMockComponent {}
