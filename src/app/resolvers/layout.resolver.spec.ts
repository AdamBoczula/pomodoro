import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { LayoutTableService } from '../db/tables/layout-table.service';

import { first } from 'rxjs/operators';
import { LayoutResolver } from './layout.resolver';

describe('LayoutResolver', () => {
  const initialState = { isDarkMode: false };
  let resolver: LayoutResolver;
  let layoutTableServiceMock: Partial<LayoutTableService>;
  let mockStore: MockStore;

  beforeEach(() => {
    layoutTableServiceMock = {
      read: jest.fn().mockReturnValue(of({ isDarkMode: false })),
    };
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({ initialState }),
        { provide: LayoutTableService, useValue: layoutTableServiceMock },
      ],
      teardown: { destroyAfterEach: false },
    });
    resolver = TestBed.inject(LayoutResolver);
    mockStore = TestBed.inject(MockStore);
  });

  beforeEach(() => {
    spyOn(mockStore, 'dispatch').and.callFake((layout) => {
      mockStore.setState(layout);
    });
  });

  test('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  test('resolve should call read on layout table service', () => {
    resolver.resolve();
    expect(layoutTableServiceMock.read).toHaveBeenCalledTimes(1);
  });

  test('resolve should call set layout', fakeAsync(() => {
    resolver.resolve().pipe(first()).subscribe();
    tick();
    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
  }));
});
