import { Injectable } from '@angular/core';

import {
 forkJoin, from, Observable
} from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ActualTaskTableService } from '../db/tables/actual-task-table.service';
import { TaskFacadeService } from '../facades/task-facade.service';
import { DoneTaskTableService } from './../db/tables/done-task-table.service';
import { ToDoTaskTableService } from './../db/tables/to-do-task-table.service';

@Injectable({
  providedIn: 'root',
})
export class TasksResolver  {
  constructor(
    private toDoTaskTableService: ToDoTaskTableService,
    private actualTaskTableService: ActualTaskTableService,
    private doneTaskTableService: DoneTaskTableService,
    private taskFacade: TaskFacadeService
  ) {}

  resolve(): Observable<boolean> {
    return forkJoin([
      from(this.toDoTaskTableService.getCollection()).pipe(
        tap(toDoTasks => {
          if (!!toDoTasks) {
            this.taskFacade.setToDoTasks(toDoTasks);
          }
        })
      ),
      from(this.actualTaskTableService.get()).pipe(
        tap(actualTask => {
          if (!!actualTask) {
            this.taskFacade.moveTaskToActual(actualTask);
          }
        })
      ),
      from(this.doneTaskTableService.getCollection()).pipe(
        tap(doneTasks => {
          if (!!doneTasks) {
            this.taskFacade.setDoneTasks(doneTasks);
          }
        })
      ),
    ]).pipe(
      map(() => {
        return true;
      })
    );
  }
}
