import { TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { TestScheduler } from 'rxjs/testing';
import { ToDoTaskTableService } from 'src/app/db/tables/to-do-task-table.service';
import { ActualTaskTableService } from './../db/tables/actual-task-table.service';
import { DoneTaskTableService } from './../db/tables/done-task-table.service';
import { generateToDoTaskTableMock } from './../db/tables/to-do-task-table.mock';
import { TaskFacadeService } from './../facades/task-facade.service';
import { TodoTask } from './../models/todo-task';
import { TasksResolver } from './tasks.resolver';

describe('TasksResolver', () => {
  let resolver: TasksResolver;
  let scheduler: TestScheduler;
  let source$;
  let expected$;
  let toDoTaskTableServiceMock: jasmine.SpyObj<any>;
  let actualTaskTableServiceMock: jasmine.SpyObj<any>;
  let doneTaskTableServiceMock: jasmine.SpyObj<any>;
  let taskFacadeMock: TaskFacadeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
    providers: [provideMockStore({}), generateToDoTaskTableMock()],
    teardown: { destroyAfterEach: false }
});
    resolver = TestBed.inject(TasksResolver);
    taskFacadeMock = TestBed.inject(TaskFacadeService);
    taskFacadeMock.setToDoTasks = jest.fn();
    taskFacadeMock.moveTaskToActual = jest.fn();
    taskFacadeMock.setDoneTasks = jest.fn();
  });

  beforeEach(() => {
    toDoTaskTableServiceMock = TestBed.inject(ToDoTaskTableService);
    actualTaskTableServiceMock = TestBed.inject(ActualTaskTableService);
    doneTaskTableServiceMock = TestBed.inject(DoneTaskTableService);
    scheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('resolve should get data fron table services and return true', () => {
    scheduler.run(({ cold, hot, expectObservable, flush }) => {
      doneTaskTableServiceMock.getCollection = jest
        .fn()
        .mockReturnValue(cold('- b |', { b: [] }));

      actualTaskTableServiceMock.get = jest
        .fn()
        .mockReturnValue(cold('-- c |', { c: null }));

      toDoTaskTableServiceMock.getCollection = jest
        .fn()
        .mockReturnValue(cold('--- d |', { d: [] }));

      const result$ = resolver.resolve();

      source$ = hot('- a', { a: result$ });
      expected$ = '- --- (z|)';

      expectObservable(result$).toBe(expected$, { z: true });

      flush();
      expect(toDoTaskTableServiceMock.getCollection).toHaveBeenCalledTimes(1);
      expect(doneTaskTableServiceMock.getCollection).toHaveBeenCalledTimes(1);
      expect(actualTaskTableServiceMock.get).toHaveBeenCalledTimes(1);
    });
  });

  it('resolve should call facade for every tasks collection when those exist', () => {
    scheduler.run(({ cold, hot, expectObservable, flush }) => {
      const task: TodoTask = {
        title: 't1',
        donePomodoroCycles: 0,
        estimate: 1,
      };
      const toDoTasks = [{ ...task }];
      const doneTasks = [{ ...task }];
      const actualTask = { ...task };

      doneTaskTableServiceMock.getCollection = jest
        .fn()
        .mockReturnValue(cold('- b |', { b: doneTasks }));

      actualTaskTableServiceMock.get = jest
        .fn()
        .mockReturnValue(cold('-- c |', { c: actualTask }));

      toDoTaskTableServiceMock.getCollection = jest
        .fn()
        .mockReturnValue(cold('--- d |', { d: toDoTasks }));

      const result$ = resolver.resolve();

      source$ = hot('- a', { a: result$ });
      expected$ = '- --- (z|)';

      expectObservable(result$).toBe(expected$, { z: true });

      flush();

      expect(taskFacadeMock.setToDoTasks).toHaveBeenCalledTimes(1);
      expect(taskFacadeMock.setToDoTasks).toHaveBeenCalledWith(toDoTasks);

      expect(taskFacadeMock.moveTaskToActual).toHaveBeenCalledTimes(1);
      expect(taskFacadeMock.moveTaskToActual).toHaveBeenCalledWith(actualTask);

      expect(taskFacadeMock.setDoneTasks).toHaveBeenCalledTimes(1);
      expect(taskFacadeMock.setDoneTasks).toHaveBeenCalledWith(doneTasks);
    });
  });
});
