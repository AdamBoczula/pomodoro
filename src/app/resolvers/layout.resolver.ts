import { Injectable } from '@angular/core';

import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LayoutTableService } from './../db/tables/layout-table.service';
import { LayoutFacadeService } from './../facades/layout-facade.service';

@Injectable({
  providedIn: 'root',
})
export class LayoutResolver  {
  constructor(
    private layoutTableService: LayoutTableService,
    private layoutFacadeService: LayoutFacadeService
  ) {}

  resolve(): Observable<boolean> {
    return from(this.layoutTableService.read()).pipe(
      map(layout => {
        if (layout) {
          this.layoutFacadeService.setLayout(layout.isDarkMode);
        } else {
          this.layoutFacadeService.setLayout(
            window.matchMedia &&
              window.matchMedia('(prefers-color-scheme: dark)').matches
          );
        }
        return true;
      })
    );
  }
}
