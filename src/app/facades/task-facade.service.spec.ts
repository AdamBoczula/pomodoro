import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TodoTask } from '../models/todo-task';
import * as ActualTaskAction from '../store/actions/actual-tasks.actions';
import * as ArchiveTaskAction from '../store/actions/archive-task.actions';
import * as DoneTaskAction from '../store/actions/done-tasks.actions';
import * as TodoTaskAction from '../store/actions/todo-tasks.actions';
import * as fromTask from '../store/reducers/task.reducer';
import { TaskFacadeService } from './task-facade.service';

describe('TaskFacadeService', () => {
  let facade: TaskFacadeService;
  let mockStore: MockStore;
  let task: TodoTask;

  beforeEach(() => {
    task = {
      title: 'Title',
      donePomodoroCycles: 0,
      estimate: 1,
      id: '99',
    };
  });
  beforeEach(() => {
    TestBed.configureTestingModule({
    providers: [provideMockStore({ initialState: fromTask.initialState })],
    teardown: { destroyAfterEach: false }
});
    facade = TestBed.inject(TaskFacadeService);
    mockStore = TestBed.inject(MockStore);
    mockStore.select = jest.fn();
    mockStore.dispatch = jest.fn();
  });

  it('should be created', () => {
    expect(facade).toBeTruthy();
  });

  test('moveTaskToDo should dispatch moveToDo action', () => {
    facade.moveTaskToDo(task);
    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      ActualTaskAction.moveToDo({ task })
    );
  });
  test('moveTaskToDone should dispatch moveToDone action', () => {
    facade.moveTaskToDone(task);
    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      ActualTaskAction.moveToDone({ task })
    );
  });

  test('moveTaskToActual should dispatch setActualTask action', () => {
    facade.moveTaskToActual(task);
    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      ActualTaskAction.setActualTask({ actualTask: task })
    );
  });

  test('moveTaskToArchive should dispatch archiveTask action', () => {
    facade.moveTaskToArchive(task);
    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      ArchiveTaskAction.archiveTask({ archiveTask: task })
    );
  });

  test('addToDoTask should dispatch addToDoTask action', () => {
    facade.addToDoTask(task);
    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      TodoTaskAction.addToDoTask({ toDoTask: task })
    );
  });

  test('setToDoTasks should dispatch setToDoTasks action', () => {
    facade.setToDoTasks([task]);
    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      TodoTaskAction.setToDoTasks({ toDoTasks: [task] })
    );
  });

  test('setDoneTasks should dispatch setDoneTasks action', () => {
    facade.setDoneTasks([task]);
    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      DoneTaskAction.setDoneTasks({ doneTasks: [task] })
    );
  });
});
