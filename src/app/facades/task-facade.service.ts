import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as ActualTaskAction from '../store/actions/actual-tasks.actions';
import * as ArchiveTaskAction from '../store/actions/archive-task.actions';
import { setDoneTasks } from '../store/actions/done-tasks.actions';
import * as fromTask from '../store/reducers/task.reducer';
import { TodoTask } from './../models/todo-task';
import {
  addToDoTask,
  setToDoTasks,
} from './../store/actions/todo-tasks.actions';
import {
  runTask,
  stopTask,
} from '../pomodoro-home/store/actions/pomodoro-home.actions';
@Injectable({
  providedIn: 'root',
})
export class TaskFacadeService {
  constructor(private store: Store<fromTask.State>) {}

  public moveTaskToDo(task: TodoTask): void {
    this.store.dispatch(ActualTaskAction.moveToDo({ task }));
  }

  public moveTaskToDone(task: TodoTask): void {
    this.store.dispatch(ActualTaskAction.moveToDone({ task }));
  }

  public moveTaskToActual(actualTask: TodoTask): void {
    this.store.dispatch(ActualTaskAction.setActualTask({ actualTask }));
  }

  public moveTaskToArchive(archiveTask: TodoTask): void {
    this.store.dispatch(ArchiveTaskAction.archiveTask({ archiveTask }));
  }

  public addToDoTask(toDoTask: TodoTask): void {
    this.store.dispatch(addToDoTask({ toDoTask }));
  }

  public setToDoTasks(toDoTasks: TodoTask[]): void {
    this.store.dispatch(setToDoTasks({ toDoTasks }));
  }

  public setDoneTasks(doneTasks: TodoTask[]): void {
    this.store.dispatch(setDoneTasks({ doneTasks }));
  }

  public runTaskPomodoro(): void {
    this.store.dispatch(runTask());
  }
  public stopTaskPomodoro(): void {
    this.store.dispatch(stopTask());
  }
}
