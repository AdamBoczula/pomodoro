import { setLightMode } from './../store/actions/layout.actions';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';

import { LayoutFacadeService } from './layout-facade.service';
import * as fromLayout from '../store/reducers/layout.reducer';
import { setDarkMode, setLayout } from '../store/actions/layout.actions';
import { map } from 'rxjs/operators';
import { selectIsDarkTheme } from '../store/selectors/layout.selectors';

describe('LayoutFacadeService', () => {
  let layoutFacade: LayoutFacadeService;
  let mockStore: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
    providers: [
        provideMockStore({
            initialState: fromLayout.initialState,
            selectors: [
                {
                    selector: selectIsDarkTheme,
                    value: fromLayout.initialState.isDarkMode,
                },
            ],
        }),
    ],
    teardown: { destroyAfterEach: false }
});
    mockStore = TestBed.inject(MockStore);
    mockStore.dispatch = jest.fn();
    layoutFacade = TestBed.inject(LayoutFacadeService);
  });

  test('should be created', () => {
    expect(layoutFacade).toBeTruthy();
  });

  test('setLayout should dispatch setLayoutAction with true', () => {
    layoutFacade.setLayout(true);
    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      setLayout({ isDarkMode: true })
    );
  });

  test('toggleApplicationMode should dispatch setDarkMode when isDarkMode is false', () => {
    mockStore.overrideSelector(selectIsDarkTheme, false);
    mockStore.refreshState();
    layoutFacade.toggleApplicationMode();

    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
    expect(mockStore.dispatch).toHaveBeenCalledWith(setDarkMode());
  });

  test('toggleApplicationMode should dispatch setLightMode when isDarkMode is true', () => {
    mockStore.overrideSelector(selectIsDarkTheme, true);
    mockStore.refreshState();
    layoutFacade.toggleApplicationMode();

    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
    expect(mockStore.dispatch).toHaveBeenCalledWith(setLightMode());
  });
});
