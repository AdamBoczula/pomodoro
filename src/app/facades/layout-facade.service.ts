import { setDarkMode, setLightMode } from './../store/actions/layout.actions';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { setLayout } from '../store/actions/layout.actions';
import * as fromLayout from '../store/reducers/layout.reducer';
import { selectIsDarkTheme } from './../store/selectors/layout.selectors';

@Injectable({
  providedIn: 'root',
})
export class LayoutFacadeService {
  constructor(private store: Store<fromLayout.State>) {}

  public setLayout(isDarkMode: boolean): void {
    this.store.dispatch(setLayout({ isDarkMode }));
  }

  public toggleApplicationMode(): void {
    this.isDarkTheme$.pipe(take(1)).subscribe(isDarkTheme => {
      isDarkTheme
        ? this.store.dispatch(setLightMode())
        : this.store.dispatch(setDarkMode());
    });
  }

  public get isDarkTheme$(): Observable<boolean> {
    return this.store.select(selectIsDarkTheme);
  }
}
