import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterOutlet } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { first } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { AppNavigationComponent } from './components/app-navigation/app-navigation.component';
import { LanguageSelectorComponent } from './components/language-selector/language-selector.component';
import { MenuSettingsContainerComponent } from './components/menu-settings-container/menu-settings-container.component';
import { ThemeSelectorComponent } from './components/theme-selector/theme-selector.component';
import { DexieWrapperService } from './db/dexie-wrapper.service';
import { ModalModule } from './modal/modal.module';
import { LayoutResolver } from './resolvers/layout.resolver';
import { metaReducers, reducers } from './store';
import { LayoutEffects } from './store/effects/layout.effects';
import { TaskEffects } from './store/effects/task.effects';
import * as fromLayout from './store/reducers/layout.reducer';
import * as fromTask from './store/reducers/task.reducer';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { getAuth, provideAuth } from '@angular/fire/auth';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { getFunctions, provideFunctions } from '@angular/fire/functions';


@NgModule({
  declarations: [
    AppComponent,
    AppNavigationComponent,
    ThemeSelectorComponent,
    LanguageSelectorComponent,
    MenuSettingsContainerComponent
  ],
  imports: [
    BrowserAnimationsModule,
    RouterOutlet, 
    BrowserModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatInputModule,
    FormsModule,
    MatSidenavModule,
    MatMenuModule,
    ModalModule,
    MatSnackBarModule,
    MatIconModule,
    MatButtonModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    StoreRouterConnectingModule.forRoot(),
    StoreModule.forRoot({}, {}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    StoreModule.forFeature(fromTask.taskFeatureKey, fromTask.reducer),
    EffectsModule.forFeature([TaskEffects, LayoutEffects]),
    StoreModule.forFeature(fromLayout.layoutFeatureKey, fromLayout.reducer),
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (layoutResolver: LayoutResolver) => {
        return () =>
          layoutResolver
            .resolve()
            .pipe(first())
            .subscribe(() => { });
      },
      multi: true,
      deps: [LayoutResolver],
    },
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: 'outline' },
    },
    provideFirebaseApp(() => initializeApp({"projectId":"boczkodoro","appId":"1:47123518867:web:ce4a4d923727bf9d5dc466","storageBucket":"boczkodoro.appspot.com","apiKey":"AIzaSyDgUZiC2a02WGkvMraRNanx3ukVVrB3eNA","authDomain":"boczkodoro.firebaseapp.com","messagingSenderId":"47123518867","measurementId":"G-VJ00CDKTFV"})),
    provideAuth(() => getAuth()),
    provideFirestore(() => getFirestore()),
    provideFunctions(() => getFunctions()),
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(private db: DexieWrapperService) { }
}
