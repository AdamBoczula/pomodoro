import { TableModelInterface } from '../db/models/table-model-interface';
export type TaskState = 'To do' | 'Actual' | 'Done';

export interface TodoTask extends TableModelInterface {
  title: string;
  id?: string;
  description?: string;
  label?: string;
  estimate?: number;
  donePomodoroCycles: number;
  state?: TaskState;
}
