'use strict';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === 'object' && typeof Reflect.decorate === 'function') r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator['throw'](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), 'throw': verb(1), 'return': verb(2) }, typeof Symbol === 'function' && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError('Generator is already executing.');
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y['return'] : op[0] ? y['throw'] || ((t = y['return']) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var pomodoro_home_actions_1 = require('./../../store/actions/pomodoro-home.actions');
var core_1 = require('@angular/core');
var testing_1 = require('@angular/core/testing');
var testing_2 = require('@ngrx/store/testing');
var pomodoro_home_actions_2 = require('../../store/actions/pomodoro-home.actions');
var fromPomodoroHome = require('../../store/reducers/pomodoro-home.reducer');
var actual_task_state_enum_1 = require('./../../models/actual-task-state.enum');
var actual_component_1 = require('./actual.component');
var actualTask;
var initialState;
describe('ActualComponent', function () {
    var component;
    var testingComponent;
    var fixture;
    var mockStore;
    beforeEach(function () {
        initialState = fromPomodoroHome.initialState;
    });
    beforeEach(function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, testing_1.TestBed.configureTestingModule({
                        declarations: [
                            actual_component_1.ActualComponent,
                            MatIconMockComponent,
                            TaskMockComponent,
                            TestActualComponent,
                        ],
                        providers: [testing_2.provideMockStore({ initialState: initialState })]
                    }).compileComponents()];
                case 1:
                    _a.sent();
                    mockStore = testing_1.TestBed.inject(testing_2.MockStore);
                    mockStore.dispatch = jest.fn();
                    return [2 /*return*/];
            }
        });
    }); });
    beforeEach(function () {
        actualTask = {
            title: 'T1',
        };
    });
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(TestActualComponent);
        component = fixture.componentInstance;
        testingComponent = fixture.debugElement.children[0].componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
    test('when taskState is Paused should display play button', function () {
        expect(fixture.nativeElement.querySelector('.play')).toBeDefined();
        expect(fixture.nativeElement.querySelector('.pause')).toBeNull();
    });
    test('when taskState is Played should display pause button', function () {
        component.taskState = actual_task_state_enum_1.ActualTaskState.PLAYED;
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('.play')).toBeNull();
        expect(fixture.nativeElement.querySelector('.pause')).toBeDefined();
    });
    test('click back button should dispatch moveBackward action', function () {
        fixture.nativeElement.querySelector('.back-todo').click();
        fixture.detectChanges();
        expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
        expect(mockStore.dispatch).toHaveBeenCalledWith(pomodoro_home_actions_2.moveBackward({ actualTask: component.actualTask }));
    });
    test('click forward button should dispatch moveForward action', function () {
        fixture.nativeElement.querySelector('.done').click();
        fixture.detectChanges();
        expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
        expect(mockStore.dispatch).toHaveBeenCalledWith(pomodoro_home_actions_2.moveForward({ actualTask: component.actualTask }));
    });
    describe('click play button', function () {
        beforeEach(function () {
            fixture.nativeElement.querySelector('.play').click();
            fixture.detectChanges();
        });
        test('should dispatch startTimer action', function () {
            expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
            expect(mockStore.dispatch).toHaveBeenCalledWith(pomodoro_home_actions_2.startTimer());
        });
        test('should change play button to pause button', function () {
            expect(fixture.nativeElement.querySelector('.pause')).toBeDefined();
            expect(fixture.nativeElement.querySelector('.play')).toBeNull();
        });
        test('should change actual task state to PLAYED', function () {
            expect(testingComponent.actualTaskState).toEqual(actual_task_state_enum_1.ActualTaskState.PLAYED);
        });
    });
    describe('click pause button', function () {
        beforeEach(function () {
            fixture.nativeElement.querySelector('.play').click();
            fixture.detectChanges();
            jest.clearAllMocks();
            fixture.nativeElement.querySelector('.pause').click();
            fixture.detectChanges();
        });
        test('should dispatch stopTimer action', function () {
            expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
            expect(mockStore.dispatch).toHaveBeenCalledWith(pomodoro_home_actions_1.stopTimer());
        });
        test('should change pause button to play button', function () {
            expect(fixture.nativeElement.querySelector('.play')).toBeDefined();
            expect(fixture.nativeElement.querySelector('.pause')).toBeNull();
        });
        test('should change actual task state to PAUSED', function () {
            expect(testingComponent.actualTaskState).toEqual(actual_task_state_enum_1.ActualTaskState.PAUSED);
        });
    });
});
var TestActualComponent = /** @class */ (function () {
    function TestActualComponent() {
        this.actualTask = actualTask;
        this.actualTaskState = actual_task_state_enum_1.ActualTaskState.PAUSED;
    }
    TestActualComponent = __decorate([
        core_1.Component({
            selector: 'test-actual',
            template: '<pom-actual [actualTask]="actualTask" [actualTaskState]="actualTaskState"></pom-actual>'
        })
    ], TestActualComponent);
    return TestActualComponent;
}());
var MatIconMockComponent = /** @class */ (function () {
    function MatIconMockComponent() {
    }
    MatIconMockComponent = __decorate([
        core_1.Component({
            selector: 'mat-icon',
            template: ''
        })
    ], MatIconMockComponent);
    return MatIconMockComponent;
}());
var TaskMockComponent = /** @class */ (function () {
    function TaskMockComponent() {
    }
    __decorate([
        core_1.Input()
    ], TaskMockComponent.prototype, 'task');
    TaskMockComponent = __decorate([
        core_1.Component({
            selector: 'pom-task',
            template: ''
        })
    ], TaskMockComponent);
    return TaskMockComponent;
}());
