'use strict';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === 'object' && typeof Reflect.decorate === 'function') r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ActualComponent = void 0;
var pomodoro_home_actions_1 = require('./../../store/actions/pomodoro-home.actions');
var core_1 = require('@angular/core');
var pomodoro_home_actions_2 = require('../../store/actions/pomodoro-home.actions');
var actual_task_state_enum_1 = require('./../../models/actual-task-state.enum');
var ActualComponent = /** @class */ (function () {
    function ActualComponent(store) {
        this.store = store;
    }
    ActualComponent.prototype.isPlaying = function () {
        return this.actualTaskState === actual_task_state_enum_1.ActualTaskState.PLAYED;
    };
    ActualComponent.prototype.backToDo = function () {
        this.store.dispatch(pomodoro_home_actions_2.moveBackward({ actualTask: this.actualTask }));
    };
    ActualComponent.prototype.moveToDone = function () {
        this.store.dispatch(pomodoro_home_actions_1.moveForward({ actualTask: this.actualTask }));
    };
    ActualComponent.prototype.play = function () {
        this.actualTaskState = actual_task_state_enum_1.ActualTaskState.PLAYED;
        this.store.dispatch(pomodoro_home_actions_1.startTimer());
    };
    ActualComponent.prototype.pause = function () {
        this.actualTaskState = actual_task_state_enum_1.ActualTaskState.PAUSED;
        this.store.dispatch(pomodoro_home_actions_1.stopTimer());
    };
    __decorate([
        core_1.Input()
    ], ActualComponent.prototype, 'actualTask');
    __decorate([
        core_1.Input()
    ], ActualComponent.prototype, 'actualTaskState');
    ActualComponent = __decorate([
        core_1.Component({
            selector: 'pom-actual',
            templateUrl: './actual.component.html',
            styleUrls: ['./actual.component.scss']
        })
    ], ActualComponent);
    return ActualComponent;
}());
exports.ActualComponent = ActualComponent;
