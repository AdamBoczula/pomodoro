import { Component, Input } from '@angular/core';
import { TodoTask } from 'src/app/models/todo-task';
import { TaskTimerService } from '../../services/task-timer.service';
import { TaskFacadeService } from './../../../facades/task-facade.service';
import { ActualTaskState } from './../../models/actual-task-state.enum';
import {
  POMODORO_TIME_CYCLE,
  SECONDS_PER_MINUTE,
} from './constants/pomodoro-time-cycle';

@Component({
  selector: 'pom-actual',
  templateUrl: './actual.component.html',
  styleUrls: ['./actual.component.scss'],
})
export class ActualComponent {
  @Input()
  public actualTask: TodoTask;
  @Input()
  public actualTaskState: ActualTaskState | null;
  public readonly pomodorCycleInMiliseconds: number;

  public constructor(
    private taskFacadeService: TaskFacadeService,
    private readonly taskTimerService: TaskTimerService
  ) {
    this.pomodorCycleInMiliseconds = POMODORO_TIME_CYCLE * SECONDS_PER_MINUTE;
  }

  public isPlaying(): boolean {
    return this.actualTaskState === 'Played';
  }

  public backToDo(): void {
    this.taskFacadeService.moveTaskToDo(this.actualTask);
    this.taskTimerService.reset();
  }

  public moveToDone(): void {
    this.taskFacadeService.moveTaskToDone(this.actualTask);
    this.taskTimerService.reset();
  }

  public play(): void {
    this.taskFacadeService.runTaskPomodoro();
    this.taskTimerService.start();
  }

  public pause(): void {
    this.taskFacadeService.stopTaskPomodoro();
    this.taskTimerService.stop();
  }
}
