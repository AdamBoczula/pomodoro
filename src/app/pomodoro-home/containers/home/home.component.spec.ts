import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDividerModule } from '@angular/material/divider';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApplicationHeightCounterService } from '../../../services/application-height-counter.service';
import * as fromTask from '../../../store/reducers/task.reducer';
import { setCreationTaskMode } from '../../store/actions/pomodoro-home.actions';
import { selectHomeMode } from '../../store/selectors/pomodoro-home.selectors';
import { DEFAULT_IMPORTS_TESTING } from './../../default-testing-imports';
import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  const initialState: fromTask.State = fromTask.initialState;
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let actions$: Observable<any>;
  let mockStore: MockStore;
  let applicationHeightCounterServiceMock: any;

  beforeEach(async () => {
    applicationHeightCounterServiceMock = {
      setGlobalHeightAppVar: jest.fn(),
    };

    await TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        TasksListMockComponent,
        PomActualMockComponent,
        PomStatusBarMockComponent,
        NewTaskFormMockComponent,
        DoneTasksMockComponent,
        TaskMockComponent,
      ],
      providers: [
        provideMockStore({ initialState }),
        provideMockActions(() => actions$),
        {
          provide: ApplicationHeightCounterService,
          useValue: applicationHeightCounterServiceMock,
        },
      ],
      imports: [...DEFAULT_IMPORTS_TESTING, MatDividerModule],
      teardown: { destroyAfterEach: false },
    }).compileComponents();
  });

  beforeEach(() => {
    mockStore = TestBed.inject(MockStore);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    mockStore = TestBed.inject(MockStore);

    spyOn(mockStore, 'dispatch').and.callFake((homeMode) => {
      mockStore.setState({ homeMode });
      mockStore.overrideSelector(selectHomeMode, 'CREATION');
      mockStore.refreshState();
    });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  test('creation mode should call dispatch setCreationTaskMode with CREATION mode', () => {
    component.createNewTask();
    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      setCreationTaskMode({ creationTaskMode: 'CREATION' })
    );
  });
  test('should create new task after click', () => {
    window.HTMLElement.prototype.scroll = function () {};

    component.createNewTask();
    fixture.detectChanges();
    expect(
      fixture.nativeElement.querySelector('pom-new-task-form')
    ).toBeTruthy();
  });

  test('should calculate height after ngViewInit', () => {
    expect(
      applicationHeightCounterServiceMock.setGlobalHeightAppVar
    ).toHaveBeenCalledTimes(1);
  });
  test('should show information about empty list when there is no todo task', () => {
    component.toDoTasks$ = new BehaviorSubject([]);
    fixture.detectChanges();
    expect(
      fixture.nativeElement.querySelector('.todo-section pom-task')
    ).toBeNull();
    expect(
      fixture.nativeElement.querySelector('.empty-todos-information')
    ).not.toBeNull();
  });
  test('should not show information about empty list when there is', () => {
    component.toDoTasks$ = new BehaviorSubject([
      { title: 't1', id: '0', spentTime: 0, state: TaskState.TODO } as TodoTask,
    ]);
    fixture.detectChanges();
    expect(
      fixture.nativeElement.querySelector('.todo-section pom-task')
    ).not.toBeNull();
    expect(
      fixture.nativeElement.querySelector('.empty-todos-information')
    ).toBeNull();
  });
});

@Component({
  selector: 'pom-tasks-list',
  template: '',
})
class TasksListMockComponent {
  @Input()
  public tasks: TodoTask[];
}
@Component({
  selector: 'pom-actual',
  template: '',
})
class PomActualMockComponent {
  @Input()
  public actualTask: TodoTask;
}

@Component({
  selector: 'pom-status-bar',
  template: '',
})
class PomStatusBarMockComponent {
  @Input()
  public spentTime: number | null;
}

@Component({
  selector: 'pom-done-tasks',
  template: '',
})
class DoneTasksMockComponent {
  @Input()
  public doneTasks: TodoTask;
}

@Component({
  selector: 'pom-new-task-form',
  template: '',
})
class NewTaskFormMockComponent {}

@Component({
  selector: 'pom-task',
  template: '',
})
class TaskMockComponent {
  @Input()
  public task: TodoTask;
}
