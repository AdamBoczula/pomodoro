import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { map, tap } from 'rxjs/operators';
import { TodoTask } from 'src/app/models/todo-task';
import { moveTopBottomAnimation } from 'src/app/store/utils/animations/top-bottom.animation';
import * as fromTasks from '../../../store/reducers/task.reducer';
import { selectActualTask } from '../../../store/selectors/task.selectors';
import { TaskTimerService } from '../../services/task-timer.service';
import { setCreationTaskMode } from '../../store/actions/pomodoro-home.actions';
import { selectHomeMode } from '../../store/selectors/pomodoro-home.selectors';
import { ApplicationHeightCounterService } from './../../../services/application-height-counter.service';
import {
  selectDoneTasks,
  selectToDoTasks,
} from './../../../store/selectors/task.selectors';
import { selectActualTaskState } from './../../store/selectors/pomodoro-home.selectors';

@Component({
  selector: 'pom-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [moveTopBottomAnimation],
})
export class HomeComponent implements AfterViewInit {
  @ViewChild('newTaskWrapper')
  public newTaskWrapper: ElementRef;
  @ViewChild('statusBar')
  public statusBar: ElementRef;

  public isCreationMode$ = this.store.pipe(
    select(selectHomeMode),
    map((homeMode) => {
      return homeMode === 'CREATION';
    }),
    tap((isHomeMode) => {
      if (isHomeMode) {
        this.newTaskWrapper.nativeElement.style.display = 'block';
      } else {
        setTimeout(
          () => (this.newTaskWrapper.nativeElement.style.display = 'none'),
          150
        );
      }
    })
  );
  public actualTask$ = this.store.pipe(
    select(selectActualTask),
    map((actualTask) => actualTask as TodoTask)
  );
  public actualTaskState$ = this.store.pipe(select(selectActualTaskState));
  public doneTasks$ = this.store.pipe(select(selectDoneTasks));
  public toDoTasks$ = this.store.pipe(select(selectToDoTasks));
  public pomodoroCycle$ = this.taskTimerService.pomodoroCycle$;
  public timeToLeft = this.taskTimerService.timeToLeft;

  constructor(
    private store: Store<fromTasks.State>,
    private applicationHeightCounter: ApplicationHeightCounterService,
    private readonly taskTimerService: TaskTimerService
  ) {}

  public ngAfterViewInit(): void {
    this.applicationHeightCounter.setGlobalHeightAppVar();
  }

  public createNewTask(): void {
    this.store.dispatch(setCreationTaskMode({ creationTaskMode: 'CREATION' }));
  }
}
