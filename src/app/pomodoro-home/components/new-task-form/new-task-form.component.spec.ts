import { PomodoroHomeFacadeService } from './../../facades/pomodoro-home-facade.service';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable } from 'rxjs';
import { Action } from 'rxjs/internal/scheduler/Action';
import { TaskState, TodoTask } from 'src/app/models/todo-task';
import { addToDoTask } from '../../../store/actions/todo-tasks.actions';
import { DEFAULT_IMPORTS_TESTING } from '../../default-testing-imports';
import { CreationTaskMode } from '../../models/creation-task-mode.enum';
import { setCreationTaskMode } from '../../store/actions/pomodoro-home.actions';
import { selectHomeMode } from '../../store/selectors/pomodoro-home.selectors';
import { NewTaskFormComponent } from './new-task-form.component';
import { TaskFacadeService } from 'src/app/facades/task-facade.service';

describe('NewTaskFormComponent', () => {
  let component: NewTaskFormComponent;
  let fixture: ComponentFixture<NewTaskFormComponent>;
  let actions$ = new Observable<Action<any>>();
  let store: MockStore;
  let pomodoroHomeFacadeMock: PomodoroHomeFacadeService;
  let taskFacadeMock: TaskFacadeService;
  const taskForm: TodoTask = {
    title: 'Title',
    description: 'Description',
    donePomodoroCycles: 0,
    label: 'Label',
    estimate: 1,
  };

  const initialState = { homeMode: CreationTaskMode.CREATION };
  beforeEach(() => {
    window.HTMLElement.prototype.scroll = function () {};
    window.HTMLElement.prototype.focus = function () {};

    spyOn(window.HTMLElement.prototype, 'scroll');
    spyOn(window.HTMLElement.prototype, 'focus');
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    declarations: [NewTaskFormComponent],
    imports: [...DEFAULT_IMPORTS_TESTING],
    providers: [
        provideMockStore({
            initialState,
            selectors: [
                { selector: selectHomeMode, value: CreationTaskMode.CREATION },
            ],
        }),
        provideMockActions(() => actions$),
    ],
    teardown: { destroyAfterEach: false }
}).compileComponents();
    store = TestBed.inject(MockStore);
    pomodoroHomeFacadeMock = TestBed.inject(PomodoroHomeFacadeService);
    pomodoroHomeFacadeMock.setCreationTaskMode = jest.fn();
    taskFacadeMock = TestBed.inject(TaskFacadeService);
    taskFacadeMock.addToDoTask = jest.fn();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTaskFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  test('should focus on new created task', fakeAsync(() => {
    tick();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(window.HTMLElement.prototype.scroll).toHaveBeenCalledTimes(1);
      expect(window.HTMLElement.prototype.focus).toHaveBeenCalledTimes(1);
    });
  }));

  test('onCancel should dispatch setCreationTaskMode with IDLE', () => {
    component.onCancel();
    expect(pomodoroHomeFacadeMock.setCreationTaskMode).toHaveBeenCalledTimes(1);
    expect(pomodoroHomeFacadeMock.setCreationTaskMode).toHaveBeenCalledWith(
      CreationTaskMode.IDLE
    );
  });

  test('onCancel should reset taskForm', () => {
    component.title.setValue('Title');
    component.onCancel();
    expect(component.title.value).toBeFalsy();
  });

  test('onCreate should dispatch createNewTask with state TODO and estimate 1 by default', () => {
    component.form.patchValue(taskForm);
    component.onCreate();

    expect(taskFacadeMock.addToDoTask).toHaveBeenCalledTimes(1);
    expect(taskFacadeMock.addToDoTask).toHaveBeenCalledWith({
      ...taskForm,
      state: TaskState.TODO,
      estimate: 1,
      donePomodoroCycles: 0,
    });
  });

  test('onCancel should reset taskForm', () => {
    component.onCancel();
    expect(component.title.value).toBeFalsy();
  });

  test('on pressed enter should call onCreate', () => {
    component.form.patchValue(taskForm);
    component.onKeyPress({ key: 'Enter' } as KeyboardEvent);

    expect(taskFacadeMock.addToDoTask).toHaveBeenCalledTimes(1);
    expect(taskFacadeMock.addToDoTask).toHaveBeenCalledWith({
      ...taskForm,
      state: TaskState.TODO,
      estimate: 1,
      donePomodoroCycles: 0,
    });
  });

  test('on pressed enter with shift should not call onCreate', () => {
    component.form.patchValue(taskForm);
    component.onKeyPress({ key: 'Enter', shiftKey: true } as KeyboardEvent);
    expect(taskFacadeMock.addToDoTask).toHaveBeenCalledTimes(0);
  });

  test('on pressed enter should reset form and dispatch IDLE when target is Cancel button', () => {
    component.form.patchValue(taskForm);
    component.onKeyPress({
      key: 'Enter',
      target: { type: 'reset' },
    } as unknown as KeyboardEvent);

    expect(component.title.value).toBeFalsy();
    expect(pomodoroHomeFacadeMock.setCreationTaskMode).toHaveBeenCalledTimes(1);
    expect(pomodoroHomeFacadeMock.setCreationTaskMode).toHaveBeenCalledWith(
      CreationTaskMode.IDLE
    );
  });
});
