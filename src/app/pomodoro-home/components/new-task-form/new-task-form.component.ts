import { Actions, ofType } from '@ngrx/effects';
import {
  delay,
  filter,
  map,
  takeUntil
} from 'rxjs/operators';
import { merge, Subject } from 'rxjs';
import { PomodoroHomeFacadeService } from './../../facades/pomodoro-home-facade.service';
import { setCreationTaskMode } from '../../store/actions/pomodoro-home.actions';
import { TaskFacadeService } from './../../../facades/task-facade.service';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
@Component({
  selector: 'pom-new-task-form',
  templateUrl: './new-task-form.component.html',
  styleUrls: ['./new-task-form.component.scss'],
})
export class NewTaskFormComponent implements AfterViewInit, OnDestroy {
  @ViewChild('titleInput')
  public titleInputEl: ElementRef;
  public titleMaxLength: number = 50;
  public labelMaxLength: number = 20;
  public estimateMin: number = 1;
  public estimateMax: number = 9;
  public destroy$: Subject<void> = new Subject();
  public form: FormGroup = new FormGroup({
    title: new FormControl('', [
      Validators.required,
      Validators.maxLength(this.titleMaxLength),
    ]),
    description: new FormControl(''),
    label: new FormControl('', Validators.maxLength(this.labelMaxLength)),
    estimate: new FormControl(undefined, [
      Validators.min(0),
      Validators.max(9),
    ]),
  });

  @HostListener('keypress', ['$event'])
  public onKeyPress(event: KeyboardEvent) {
    if (event.key.toLowerCase() === 'enter' && !event.shiftKey) {
      (event.target as HTMLButtonElement)?.type === 'reset'
        ? this.onCancel()
        : this.onCreate();
    }
  }

  public get title(): AbstractControl {
    return this.form.get('title') as AbstractControl;
  }

  public get description(): AbstractControl {
    return this.form.get('description') as AbstractControl;
  }

  public get label(): AbstractControl {
    return this.form.get('label') as AbstractControl;
  }

  public get estimate(): AbstractControl {
    return this.form.get('estimate') as AbstractControl;
  }

  public constructor(
    private cd: ChangeDetectorRef,
    private pomodoroHomeFacadeService: PomodoroHomeFacadeService,
    private taskFacadeService: TaskFacadeService,
    private actions$: Actions
  ) { }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public ngAfterViewInit(): void {
    merge(
      this.actions$.pipe(
        ofType(setCreationTaskMode),
        takeUntil(this.destroy$),
        map(({ creationTaskMode: homeMode }) => homeMode)
      ),
      this.pomodoroHomeFacadeService.homeMode$
    )
      .pipe(
        filter(homeMode => {
          return homeMode === 'CREATION';
        }),
        delay(250)
      )
      .subscribe(() => {
        this.titleInputEl.nativeElement.scroll({ behavior: 'smooth' });
        this.titleInputEl.nativeElement.focus();
        this.cd.detectChanges();
      });
  }

  public onCancel(): void {
    this.form.reset();
    this.pomodoroHomeFacadeService.setCreationTaskMode('IDLE');
  }

  public onCreate(): void {
    if (this.form.valid) {
      this.taskFacadeService.addToDoTask({
        ...this.form.value,
        state: 'To do',
        estimate: this.estimate.value || 1,
        donePomodoroCycles: 0,
      });
      this.closeAndReset();
    }
  }

  private closeAndReset(): void {
    this.form.reset();
  }
}
