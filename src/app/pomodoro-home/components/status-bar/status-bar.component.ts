import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'pom-status-bar',
  templateUrl: './status-bar.component.html',
  styleUrls: ['./status-bar.component.scss'],
})
export class StatusBarComponent implements OnChanges {
  @Input()
  public cycleTime: number;

  @Input()
  public leftTime: number | null;

  public statusBarPercentagePosition: number = 0;

  public constructor() {}

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.leftTime?.currentValue) {
      this.statusBarPercentagePosition =
        100 - (this.leftTime! * 100) / this.cycleTime;
    } else {
      this.statusBarPercentagePosition = 0;
      this.leftTime = this.cycleTime;
    }
  }
}
