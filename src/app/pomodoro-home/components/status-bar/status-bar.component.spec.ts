import { Component, Input, SimpleChanges } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TimePipe } from 'src/app/time.pipe';
import { StatusBarComponent } from './status-bar.component';

let spentTime: number | undefined;

describe('StatusBarComponent', () => {
  let component: StatusBarComponent;
  let fixture: ComponentFixture<TestingNewTaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    declarations: [
        StatusBarComponent,
        TestingNewTaskComponent,
        TimePipe,
        MatProgressBarMockComponent,
    ],
    teardown: { destroyAfterEach: false }
}).compileComponents();
  });

  beforeEach(() => {
    spentTime = 300000;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingNewTaskComponent);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });
  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('hen actual task is selected set statusBarPercentagePosition', () => {
    expect(component.statusBarPercentagePosition).toBe(20); // 5min from 25min is 20%
  });
  test('when actual task is removed should status bar position be 0', () => {
    fixture.nativeElement.actualTask = undefined;
    component.statusBarPercentagePosition = 10;
    component.ngOnChanges({
      spentTime: {
        currentValue: undefined,
        previousValue: spentTime,
      } as unknown as SimpleChanges,
    });
    expect(component.statusBarPercentagePosition).toBe(0);
  });
});

@Component({
  selector: 'testing-component',
  template: '<pom-status-bar [spentTime]="spentTime"></pom-status-bar>',
})
class TestingNewTaskComponent {
  public spentTime: number | undefined = spentTime;
}
@Component({ selector: 'mat-progress-bar', template: '' })
class MatProgressBarMockComponent {
  @Input()
  public value: number;
}
