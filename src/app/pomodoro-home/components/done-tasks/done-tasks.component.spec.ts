import { TaskState, TodoTask } from 'src/app/models/todo-task';
import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoneTasksComponent } from './done-tasks.component';

let doneTasks: TodoTask[];

describe('DoneTasksComponent', () => {
  let component: TestingDoneTasksComponent;
  let testingComponent: DoneTasksComponent;

  let fixture: ComponentFixture<TestingDoneTasksComponent>;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
    declarations: [
        DoneTasksComponent,
        TestingDoneTasksComponent,
        TestingTaskComponent,
    ],
    teardown: { destroyAfterEach: false }
}).compileComponents();
  });

  beforeEach(() => {
    doneTasks = [
      {
        title: 'T1',
        description: 'D1',
        state: TaskState.DONE,
      },
    ];
    fixture = TestBed.createComponent(TestingDoneTasksComponent);
    component = fixture.componentInstance;
    testingComponent = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  test('should contains one task', () => {
    expect(fixture.nativeElement.querySelectorAll('pom-task').length).toEqual(
      1
    );
  });
});

@Component({
  selector: '',
  template: '<pom-done-tasks [doneTasks]="doneTasks"></pom-done-tasks>',
})
class TestingDoneTasksComponent {
  public doneTasks = doneTasks;
}

@Component({
  selector: 'pom-task',
  template: '',
})
class TestingTaskComponent {
  @Input()
  public task: TodoTask;
}
