import { Component, Input } from '@angular/core';
import { TodoTask } from 'src/app/models/todo-task';

@Component({
  selector: 'pom-done-tasks',
  templateUrl: './done-tasks.component.html',
  styleUrls: ['./done-tasks.component.scss'],
})
export class DoneTasksComponent {
  @Input()
  public doneTasks: TodoTask[] | null;
}
