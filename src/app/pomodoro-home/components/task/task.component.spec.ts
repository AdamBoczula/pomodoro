import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { provideMockStore } from '@ngrx/store/testing';
import { TodoTask } from './../../../models/todo-task';
import { TaskComponent } from './task.component';
import { TaskFacadeService } from '../../../facades/task-facade.service';

describe('TaskComponent', () => {
  let component: TaskComponent;
  let fixture: ComponentFixture<TaskTestingComponent>;
  let taskFacadeMock: TaskFacadeService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    declarations: [TaskComponent, TaskTestingComponent, MatIconMock],
    imports: [MatCardModule, NoopAnimationsModule, MatMenuModule],
    providers: [provideMockStore()],
    teardown: { destroyAfterEach: false }
}).compileComponents();
  });

  beforeEach(() => {
    taskFacadeMock = TestBed.inject(TaskFacadeService);
    taskFacadeMock.moveTaskToActual = jest.fn();
    taskFacadeMock.moveTaskToDo = jest.fn();
    taskFacadeMock.moveTaskToArchive = jest.fn();
    taskFacadeMock.moveTaskToDone = jest.fn();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskTestingComponent);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('should contains 1 tomatoe filled, and 1 tomatoe left', () => {
    expect(
      fixture.nativeElement.querySelector('.task-estimation').childElementCount
    ).toEqual(component.task.estimate);
    expect(
      fixture.nativeElement.querySelectorAll(
        '.task-estimation>.estimation-left'
      ).length
    ).toBe(1);
    expect(
      fixture.nativeElement.querySelectorAll('.task-estimation>.done-cycle')
        .length
    ).toBe(1);
  });

  test('should not contains tomatoes when estimate is null', () => {
    component.task.estimate = undefined;
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('.task-estimation')).toBeNull();
  });

  test('should not contains label when label is empty', () => {
    component.task.label = undefined;
    fixture.detectChanges();
    expect(
      fixture.nativeElement.querySelector('.task-label').childElementCount
    ).toEqual(0);
  });

  describe('todo task', () => {
    test('should contain button', () => {
      fixture.detectChanges();
      expect(
        fixture.nativeElement.querySelector('mat-card-actions>button').innerHTML
      ).toEqual('Go In Progress 💪');
    });

    test('click should dispatch setActualTask action', () => {
      fixture.nativeElement.querySelector('mat-card-actions>button').click();
      fixture.detectChanges();
      expect(taskFacadeMock.moveTaskToActual).toHaveBeenCalledTimes(1);
      expect(taskFacadeMock.moveTaskToActual).toHaveBeenCalledWith(
        component.task
      );
    });

    describe('after click menu', () => {
      beforeEach(() => {
        fixture.nativeElement.querySelector('.task-menu').click();
        fixture.detectChanges();
      });
      test('should shows 4 options', () => {
        expect(
          fixture.nativeElement.parentNode.querySelectorAll(
            '.mat-menu-panel button'
          ).length
        ).toBe(4);
      });
      test('should shows disabled move to do task', () => {
        expect(
          fixture.nativeElement.parentNode.querySelector('.move-todo').disabled
        ).toBeTruthy();
      });
      test('should shows enabled move to actual, done and archive task', () => {
        expect(
          fixture.nativeElement.parentNode.querySelector('.move-actual')
            .disabled
        ).toBeFalsy();
        expect(
          fixture.nativeElement.parentNode.querySelector('.move-done').disabled
        ).toBeFalsy();
        expect(
          fixture.nativeElement.parentNode.querySelector('.move-archive')
            .disabled
        ).toBeFalsy();
      });
      test('click move to do should do nothing', () => {
        fixture.nativeElement.parentNode.querySelector('.move-todo').click();
        expect(taskFacadeMock.moveTaskToDo).not.toHaveBeenCalled();
      });
      test('click move actual should moveTaskToActual', () => {
        fixture.nativeElement.parentNode.querySelector('.move-actual').click();
        expect(taskFacadeMock.moveTaskToActual).toHaveBeenCalledTimes(1);
        expect(taskFacadeMock.moveTaskToActual).toHaveBeenCalledWith(
          component.task
        );
      });
      test('click move done should moveTaskToDone', () => {
        fixture.nativeElement.parentNode.querySelector('.move-done').click();
        expect(taskFacadeMock.moveTaskToDone).toHaveBeenCalledTimes(1);
        expect(taskFacadeMock.moveTaskToDone).toHaveBeenCalledWith(
          component.task
        );
      });
      test('click move archive should moveTaskToArchive', () => {
        fixture.nativeElement.parentNode.querySelector('.move-archive').click();
        expect(taskFacadeMock.moveTaskToArchive).toHaveBeenCalledTimes(1);
        expect(taskFacadeMock.moveTaskToArchive).toHaveBeenCalledWith(
          component.task
        );
      });
    });
  });

  describe('done task', () => {
    beforeEach(() => {
      component.task.state = TaskState.DONE;
      fixture.detectChanges();
    });
    test('should contain button', () => {
      expect(
        fixture.nativeElement.querySelector('mat-card-actions>button').innerHTML
      ).toEqual('Archive Task 📦');
    });

    test('click should moveTaskToArchive', () => {
      fixture.nativeElement.querySelector('mat-card-actions>button').click();
      fixture.detectChanges();
      expect(taskFacadeMock.moveTaskToArchive).toHaveBeenCalledTimes(1);
      expect(taskFacadeMock.moveTaskToArchive).toHaveBeenCalledWith(
        component.task
      );
    });

    describe('after click menu', () => {
      beforeEach(() => {
        fixture.nativeElement.querySelector('.task-menu').click();
        fixture.detectChanges();
      });
      test('should shows 4 options', () => {
        expect(
          fixture.nativeElement.parentNode.querySelectorAll(
            '.mat-menu-panel button'
          ).length
        ).toBe(4);
      });
      test('should shows disabled move done', () => {
        expect(
          fixture.nativeElement.parentNode.querySelector('.move-done').disabled
        ).toBeTruthy();
      });
      test('click move to done should do nothing', () => {
        fixture.nativeElement.parentNode.querySelector('.move-todo').click();
        fixture.detectChanges();
        expect(taskFacadeMock.moveTaskToDone).not.toHaveBeenCalled();
      });
    });
  });

  describe('actual task', () => {
    beforeEach(() => {
      beforeEach(() => {
        component.task.state = TaskState.ACTUAL;
        fixture.detectChanges();
      });
      test('should not contain button', () => {
        expect(
          fixture.nativeElement.querySelector('mat-card-actions>button')
        ).toBeNull();
      });

      describe('after click menu', () => {
        beforeEach(() => {
          fixture.nativeElement.querySelector('.task-menu').click();
          fixture.detectChanges();
        });
        test('should shows 4 options', () => {
          expect(
            fixture.nativeElement.parentNode.querySelectorAll(
              '.mat-menu-panel button'
            ).length
          ).toBe(4);
        });
        test('should shows disabled move done', () => {
          expect(
            fixture.nativeElement.parentNode.querySelector('.move-actual')
              .disabled
          ).toBeTruthy();
        });
        test('click move done should do nothing', () => {
          fixture.nativeElement.parentNode.querySelector('.move-todo').click();
          fixture.detectChanges();
          expect(taskFacadeMock.moveTaskToDone).not.toHaveBeenCalled();
        });
      });
    });
  });
});

@Component({
  selector: 'task-testing-component',
  template: '<pom-task [task]="task"></pom-task>',
})
class TaskTestingComponent {
  public task: TodoTask = {
    title: 'title',
    description: 'description',
    estimate: 2,
    donePomodoroCycles: 1,
    label: 'label',
    state: TaskState.TODO,
  };
}

@Component({
  selector: 'mat-icon',
  template: '',
})
class MatIconMock {}
