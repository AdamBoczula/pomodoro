import { moveRightLeftAnimation } from './../../../store/utils/animations/left-right.animation';
import { TaskFacadeService } from './../../../facades/task-facade.service';
import { TodoTask } from 'src/app/models/todo-task';
import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'pom-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
  animations: [moveRightLeftAnimation],
})
export class TaskComponent implements OnInit, OnDestroy, OnChanges {
  @Input()
  public task: TodoTask;
  public created: boolean = false;

  public constructor(private taskFacadeService: TaskFacadeService) { }
  public ngOnInit(): void {
    setTimeout(() => {
      this.created = true;
    });
  }
  public ngOnDestroy(): void {
    this.created = false;
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (!changes.task.firstChange) {
      this.task = changes.task.previousValue;
      this.created = false;
      setTimeout(() => {
        this.created = true;
        this.task = changes.task.currentValue;
      }, 250);
    }
  }

  public moveToActualTask(): void {
    this.taskFacadeService.moveTaskToActual(this.task);
  }

  public moveToDoTask(): void {
    this.taskFacadeService.moveTaskToDo(this.task);
  }
  public moveArchiveTask(): void {
    this.taskFacadeService.moveTaskToArchive(this.task);
  }

  public moveToDone(): void {
    this.taskFacadeService.moveTaskToDone(this.task);
  }

  public get isToDoTask(): boolean {
    return this.task.state === 'To do';
  }

  public get isActualTask(): boolean {
    return this.task.state === 'Actual';
  }

  public get isDoneTask(): boolean {
    return this.task.state === 'Done';
  }

  public get leftEstimation(): number {
    const estimationDiff = this.task.estimate! - this.task.donePomodoroCycles;
    return estimationDiff >= 0 ? estimationDiff : 0;
  }
}
