import { Injectable, signal, WritableSignal } from '@angular/core';
import { Store } from '@ngrx/store';
import { interval, Subscription } from 'rxjs';
import { filter, take, takeLast } from 'rxjs/operators';
import { updateDonePomodoroCycle } from 'src/app/store/actions/actual-tasks.actions';
import { updatePomodoroCycle } from 'src/app/store/actions/layout.actions';
import { selectPomodoroCycle } from 'src/app/store/selectors/layout.selectors';
import * as fromTask from '../../store/reducers/task.reducer';
import { ActualTaskState } from '../models/actual-task-state.enum';
import { selectActualTask } from './../../store/selectors/task.selectors';
import {
  MILISECONDS_PER_SECOND,
  SECONDS_PER_MINUTE,
} from './../containers/actual/constants/pomodoro-time-cycle';
@Injectable({
  providedIn: 'root',
})
export class TaskTimerService {
  private stopTimer$: Subscription;
  private worker: Worker | null = null;
  public pomodoroCycle$ = this.store.select(selectPomodoroCycle);
  public timeToLeft: WritableSignal<number> = signal(0);

  constructor(private store: Store<fromTask.State>) {
    let actualCycle = 0;
    this.pomodoroCycle$.subscribe((pomodoroCycle: number) => {
      this.worker?.postMessage('Paused' as ActualTaskState);
      this.timeToLeft.set(pomodoroCycle);
      actualCycle = pomodoroCycle;
    });

    if (window.Worker) {
      this.worker = new Worker(
        new URL('./timer-worker.worker', import.meta.url)
      );
      this.worker.onmessage = () => {
        const actualTime = this.timeToLeft() - MILISECONDS_PER_SECOND;

        if (actualTime <= 0) {
          this.stop();
          this.timeToLeft.set(actualCycle);
          this.store.dispatch(updateDonePomodoroCycle());
          this.worker && this.worker.postMessage('Paused');
        } else {
          this.timeToLeft.set(actualTime);
        }
      };
    }
  }

  public start(): void {
    if (this.worker) {
      this.worker.postMessage('Played' as ActualTaskState);
    } else {
      this.stopTimer$ = interval(MILISECONDS_PER_SECOND).subscribe(() => {
        this.store
          .select(selectActualTask)
          .pipe(
            filter((actualTask) => !!actualTask),
            take(1)
          )
          .subscribe(() => {});
      });
    }
  }

  public stop(): void {
    if (this.worker) {
      this.worker.postMessage('Paused' as ActualTaskState);
    }
    this.stopTimer$?.unsubscribe();
  }

  public reset(): void {
    this.stop();
    this.pomodoroCycle$
      .pipe(take(1))
      .subscribe((pomodoroCycle) => this.timeToLeft.set(pomodoroCycle));
  }

  public changePomodoroCycleTime(time: number): void {
    this.store.dispatch(
      updatePomodoroCycle({
        pomodoroCycle: time * MILISECONDS_PER_SECOND * SECONDS_PER_MINUTE,
      })
    );
  }
}
