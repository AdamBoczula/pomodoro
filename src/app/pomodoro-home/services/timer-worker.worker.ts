/// <reference lib="webworker" />

import Dexie from 'dexie';
import { MILISECONDS_PER_SECOND } from '../containers/actual/constants/pomodoro-time-cycle';
import { ActualTaskState } from '../models/actual-task-state.enum';

let timerId = 0;

addEventListener('message', ({ data }) => {
  if ((data as ActualTaskState) === 'Played') {
    let tickedSeconds = 0;
    timerId = setInterval(() => {
      tickedSeconds++;
      postMessage(tickedSeconds);
    }, MILISECONDS_PER_SECOND);
  } else {
    clearInterval(timerId);
  }
});
