import { MILISECONDS_PER_SECOND } from './../containers/actual/constants/pomodoro-time-cycle';
import { TodoTask } from 'src/app/models/todo-task';
import { selectActualTask } from './../../store/selectors/task.selectors';
import { TestScheduler } from 'rxjs/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { TaskTimerService } from './task-timer.service';
import * as fromTask from '../../store/reducers/task.reducer';
import { updateSpentTime } from '../store/actions/pomodoro-home.actions';

describe('TaskTimerService', () => {
  let initialState;
  let service: TaskTimerService;
  let scheduler: TestScheduler;
  let storeMock: MockStore;
  let actualTask: TodoTask;
  beforeEach(() => {
    actualTask = {
      title: 'T1',
      id: '0',
    };

    initialState = { ...fromTask.initialState, actualTask };
    TestBed.configureTestingModule({
    providers: [
        provideMockStore({
            initialState,
            selectors: [
                { selector: selectActualTask, value: initialState.actualTask },
            ],
        }),
    ],
    teardown: { destroyAfterEach: false }
});
    service = TestBed.inject(TaskTimerService);
  });
  beforeEach(() => {
    storeMock = TestBed.inject(MockStore);
    scheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
  });

  beforeEach(() => {
    storeMock.dispatch = jest.fn();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('start', () => {
    test('should dispatch updateSpentTime with updated spentTime, once per second', fakeAsync(() => {
      service.start();
      tick(1000);
      service.stop();
      tick(1000);
      expect(storeMock.dispatch).toHaveBeenCalledTimes(1);
      expect(storeMock.dispatch).toHaveBeenCalledWith(updateSpentTime());
    }));
  });
});
