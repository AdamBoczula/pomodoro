import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { CreationTaskMode } from '../models/creation-task-mode.enum';
import * as PomodoroHomeActions from '../store/actions/pomodoro-home.actions';
import * as fromPomodoroHome from '../store/reducers/pomodoro-home.reducer';
import {
  selectActualTaskState,
  selectHomeMode,
} from '../store/selectors/pomodoro-home.selectors';
import { TodoTask } from './../../models/todo-task';
import { PomodoroHomeFacadeService } from './pomodoro-home-facade.service';

describe('PomodoroHomeFacadeService', () => {
  let facade: PomodoroHomeFacadeService;
  let mockStore: MockStore;

  beforeEach(() => {
    task = {
      title: 'Title',
      donePomodoroCycles: 0,
      estimate: 1,
      id: '99',
    };
  });
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({ initialState: fromPomodoroHome.initialState }),
      ],
      teardown: { destroyAfterEach: false },
    });
    facade = TestBed.inject(PomodoroHomeFacadeService);
    mockStore = TestBed.inject(MockStore);
    mockStore.dispatch = jest.fn();
    mockStore.select = jest.fn();
  });

  it('should be created', () => {
    expect(facade).toBeTruthy();
  });

  test('startTimer should dispatch startTimer action', () => {
    facade.startTimer();
    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
  });
  test('stopTimer should dispatch stopTimer action', () => {
    facade.stopTimer();
    expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
  });

  test('homeMode$ should selectHomeMode from store', () => {
    const result = facade.homeMode$;
    expect(mockStore.select).toHaveBeenCalledTimes(1);
    expect(mockStore.select).toHaveBeenCalledWith(selectHomeMode);
  });

  test('homeMode$ should selectActualTaskState from store', () => {
    const result = facade.actualTaskState$;
    expect(mockStore.select).toHaveBeenCalledTimes(1);
    expect(mockStore.select).toHaveBeenCalledWith(selectActualTaskState);
  });
});
