import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CreationTaskMode } from '../models/creation-task-mode.enum';
import * as PomodoroHomeActions from '../store/actions/pomodoro-home.actions';
import * as fromPomodoroHome from '../store/reducers/pomodoro-home.reducer';
import { selectHomeMode } from '../store/selectors/pomodoro-home.selectors';
import { ActualTaskState } from './../models/actual-task-state.enum';
import { selectActualTaskState } from './../store/selectors/pomodoro-home.selectors';

@Injectable({
  providedIn: 'root',
})
export class PomodoroHomeFacadeService {
  constructor(private store: Store<fromPomodoroHome.State>) {}

  public setCreationTaskMode(creationTaskMode: CreationTaskMode): void {
    this.store.dispatch(
      PomodoroHomeActions.setCreationTaskMode({ creationTaskMode })
    );
  }

  public get homeMode$(): Observable<CreationTaskMode> {
    return this.store.select(selectHomeMode);
  }

  public get actualTaskState$(): Observable<ActualTaskState> {
    return this.store.select(selectActualTaskState);
  }
}
