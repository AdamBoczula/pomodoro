import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule  } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

export const DEFAULT_IMPORTS_TESTING = [
  MatCardModule,
  MatFormFieldModule,
  ReactiveFormsModule,
  MatInputModule,
  NoopAnimationsModule,
];
