import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromPomodoroHome from '../index';

export const pomodoroHomeFeatureSelector =
  createFeatureSelector<fromPomodoroHome.State>(
    fromPomodoroHome.pomodoroHomeFeatureKey
  );

export const selectHomeMode = createSelector(
  pomodoroHomeFeatureSelector,
  (state) => state.pomodoroHome.creationTaskMode
);

export const selectActualTaskState = createSelector(
  pomodoroHomeFeatureSelector,
  (state) => state.pomodoroHome.actualTaskState
);
