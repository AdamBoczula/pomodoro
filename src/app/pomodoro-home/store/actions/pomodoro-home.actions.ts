import { createAction, props } from '@ngrx/store';
import { CreationTaskMode } from './../../models/creation-task-mode.enum';

export const setCreationTaskMode = createAction(
  '[PomodoroHome] Set Creation Task Mode',
  props<{ creationTaskMode: CreationTaskMode }>()
);
export const runTask = createAction('[PomodoroHome] Run Task');
export const stopTask = createAction('[PomodoroHome] Stop Task');
