import { Action } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { archiveTaskSuccess } from 'src/app/store/actions/archive-task.actions';
import { Injectable } from '@angular/core';
import {
  moveToDo,
  moveToDone,
  updateDonePomodoroCycle,
} from 'src/app/store/actions/actual-tasks.actions';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { TaskTimerService } from '../../services/task-timer.service';

@Injectable()
export class PomodoroHomeEffects {
  public stopTimer$: Observable<Action> = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          moveToDone,
          moveToDo,
          archiveTaskSuccess,
          updateDonePomodoroCycle
        ),
        tap(() => {
          this.taskTimerService.stop();
        })
      ),
    { dispatch: false }
  );

  public resetTimer$: Observable<Action> = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          moveToDo,
          moveToDone,
          archiveTaskSuccess,
          updateDonePomodoroCycle
        ),
        tap(() => {
          this.taskTimerService.reset();
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private taskTimerService: TaskTimerService
  ) {}
}
