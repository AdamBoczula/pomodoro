import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { TodoTask } from '../../../models/todo-task';
import { TaskTimerService } from './../../services/task-timer.service';

import { PomodoroHomeEffects } from './pomodoro-home.effects';

describe('PomodoroHomeEffects', () => {
  let actions$: Observable<any>;
  let pomodoroHomeEffects: PomodoroHomeEffects;
  let scheduler: TestScheduler;
  let taskTimerServiceMock: jasmine.SpyObj<any>;
  let actualTask: TodoTask;
  let mockStore: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PomodoroHomeEffects,
        provideMockActions(() => actions$),
        provideMockStore(),
      ],
      teardown: { destroyAfterEach: false },
    });

    mockStore = TestBed.inject(MockStore);
    mockStore.dispatch = jest.fn();
    taskTimerServiceMock = TestBed.inject(TaskTimerService);
    pomodoroHomeEffects = TestBed.inject(PomodoroHomeEffects);
  });

  beforeEach(() => {
    taskTimerServiceMock.start = jest.fn();
    taskTimerServiceMock.stop = jest.fn();
  });

  beforeEach(() => {
    scheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
  });

  beforeEach(() => {
    actualTask = {
      title: 'T1',
      description: 'D1',
      id: '0',
      donePomodoroCycles: 1,
      state: 'Actual',
    } as const;
  });
  test('should be created', () => {
    expect(pomodoroHomeEffects).toBeTruthy();
  });
});
