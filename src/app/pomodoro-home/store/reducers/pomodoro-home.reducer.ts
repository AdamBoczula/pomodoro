import { createReducer, on } from '@ngrx/store';
import { addToDoTask } from 'src/app/store/actions/todo-tasks.actions';
import { ActualTaskState } from '../../models/actual-task-state.enum';
import { CreationTaskMode } from '../../models/creation-task-mode.enum';

import { updatePomodoroCycle } from 'src/app/store/actions/layout.actions';
import {
  moveToDo,
  moveToDone,
  setActualTaskSuccess,
  updateDonePomodoroCycle,
} from './../../../store/actions/actual-tasks.actions';
import {
  runTask,
  setCreationTaskMode,
  stopTask,
} from './../actions/pomodoro-home.actions';

export const pomodoroHomeFeatureKey = 'pomodoroHome';

export interface State {
  creationTaskMode: CreationTaskMode;
  actualTaskState: ActualTaskState;
}

export const initialState: State = {
  creationTaskMode: 'IDLE',
  actualTaskState: 'Paused',
};

export const reducer = createReducer(
  initialState,
  on(
    addToDoTask,
    (state) =>
      ({
        ...state,
        creationTaskMode: 'IDLE',
      } as const)
  ),
  on(setCreationTaskMode, (state, { creationTaskMode }) => ({
    ...state,
    creationTaskMode,
  })),
  on(
    setActualTaskSuccess,
    moveToDo,
    moveToDone,
    updateDonePomodoroCycle,
    stopTask,
    updatePomodoroCycle,
    (state) =>
      ({
        ...state,
        actualTaskState: 'Paused',
      } as const)
  ),
  on(runTask, (state) => ({ ...state, actualTaskState: 'Played' } as const))
);
