import {
 ActionReducer, ActionReducerMap, createReducer
} from '@ngrx/store';
import * as fromPomodoroHome from './reducers/pomodoro-home.reducer';

export const pomodoroHomeFeatureKey = 'pomodoroApp';

export interface State {
  [fromPomodoroHome.pomodoroHomeFeatureKey]: fromPomodoroHome.State;
}

export const initialState: State = {
  [fromPomodoroHome.pomodoroHomeFeatureKey]: fromPomodoroHome.initialState,
};

export const reducers: ActionReducerMap<State> = {
  [fromPomodoroHome.pomodoroHomeFeatureKey]: fromPomodoroHome.reducer,
};

export const reducer: ActionReducer<State> = createReducer(initialState);
