import * as fromState from './store';
import { ActualComponent } from './containers/actual/actual.component';
import { CommonModule } from '@angular/common';
import { DoneTasksComponent } from './components/done-tasks/done-tasks.component';
import { EffectsModule } from '@ngrx/effects';
import { HomeComponent } from './containers/home/home.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule  } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule} from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { NewTaskFormComponent } from './components/new-task-form/new-task-form.component';
import { NgModule } from '@angular/core';
import { PomodoroHomeEffects } from './store/effects/pomodoro-home.effects';
import { PomodoroHomeRoutingModule } from './pomodoro-home-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { StatusBarComponent } from './components/status-bar/status-bar.component';
import { StoreModule } from '@ngrx/store';
import { TaskComponent } from './components/task/task.component';
import { TimePipe } from './../time.pipe';

@NgModule({
  declarations: [
    HomeComponent,
    NewTaskFormComponent,
    TaskComponent,
    ActualComponent,
    StatusBarComponent,
    DoneTasksComponent,
    TimePipe,
  ],
  imports: [
    CommonModule,
    PomodoroHomeRoutingModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatDividerModule,
    MatInputModule,
    MatIconModule,
    MatProgressBarModule,
    MatMenuModule,
    ReactiveFormsModule,
    StoreModule.forFeature(
      fromState.pomodoroHomeFeatureKey,
      fromState.reducers
    ),
    EffectsModule.forFeature([PomodoroHomeEffects]),
  ],
})
export class PomodoroHomeModule { }
